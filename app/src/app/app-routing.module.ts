import {RouterModule, Routes} from "@angular/router";
import {NgModule} from "@angular/core";
import {ArticlePageComponent} from './layout/articleFeed/articlePage/articlePage.component';
import {FeedComponent} from './layout/articleFeed/feed.component';
import {CreateArticleComponent} from './layout/article/createArticle/createArticle.component';
import {LoginComponent} from './layout/auth/login/login.component';
import {DetailArticleComponent} from './layout/article/detailArticle/detailArticle.component';
import {RegisterComponent} from './layout/auth/register/register.component';
import {UserProfileComponent} from './layout/user/userProfile/userProfile.component';
import {UsersPageComponent} from "./layout/user/usersPage/usersPage.component";
import {ArticleControlComponent} from "./layout/article/articleControl/articleControl.component";
import {EditArticleComponent} from "./layout/article/editArticle/editArticle.component";
import {UserCabinetComponent} from "./layout/user/userCabinet/userCabinet.component";
import {DetailCategoryComponent} from "./layout/category/detailCategory/detailCategory.component";
import {ChatComponent} from "./layout/user/chat/chat.component";
import {UpdateUserComponent} from "./layout/user/userCabinet/updateUser/updateUser.component";
import {BlockUserComponent} from "./layout/user/userCabinet/blockUser/block-user.component";
import {UpdateUserRoleComponent} from "./layout/user/userCabinet/changeRole/update-user-role.component";

const routes: Routes = [
  { path: '',  redirectTo: 'articles', pathMatch: 'full'},
  {
    path: '',  component: FeedComponent,
    children: [
      {path: 'articles', component: ArticlePageComponent },
      { path: 'article/:id', component: DetailArticleComponent},
      { path: 'u/:userName', component: UserProfileComponent },
      { path: 'c/:slug', component: DetailCategoryComponent}
    ]
  },
  { path: 'create/article', component: CreateArticleComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'users', component: UsersPageComponent},
  { path: 'article/edit/:id', component: EditArticleComponent },
  {
    path: 'cabinet', component: UserCabinetComponent,
    children: [
      { path: 'articles', component: ArticleControlComponent },
      { path: 'update', component: UpdateUserComponent },
      { path: 'users/block', component: BlockUserComponent },
      { path: 'role', component: UpdateUserRoleComponent }
    ]
  },
  { path: 'chat', component: ChatComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
