import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {LoginResponse} from '../dtos/userDto';

@Injectable({
  providedIn: 'root'
})

export class AuthService {
  constructor(private http: HttpClient) {}

  public register(form: any){
    return this.http.post(environment.apiUri + '/api/User/register', form);
  }

  public login(form: any): Observable<LoginResponse>{
    return this.http.post<LoginResponse>(environment.apiUri + '/api/User/login', form)
      .pipe(
        tap((response) => {
          localStorage.setItem('token', response.token);
          localStorage.setItem('userId', response.userId)
        })
      );
  }
}
