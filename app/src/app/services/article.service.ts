import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {ListItemArticleDto, CreateArticleDto, ArticleDto} from '../dtos/listItemArticleDto';
import {environment} from '../../environments/environment';
import {listOfItemsWithPaging} from '../dtos/commonDto';
import {ArticleSortingRequestDto} from '../dtos/listItemArticleDto';
import {CommonService} from "./common.service";

class CookieService {
}

@Injectable({
  providedIn: 'root'
})

export class ArticleService{
  constructor(private http: HttpClient,
              private commonService: CommonService) {}

  public createArticle(article: CreateArticleDto): Observable<string>{
    return this.http
      .post<string>(environment.apiUri + '/api/Article', article);
  }

  public getArticles(queryParams: any): Observable<listOfItemsWithPaging<ListItemArticleDto>>{
    let params = this.commonService.configureParams(queryParams);
    return this.http
      .get<listOfItemsWithPaging<ListItemArticleDto>>(environment.apiUri + '/api/Article', {params});
  }

  public getArticleById(id: string): Observable<ArticleDto>{
    return this.http.get<ArticleDto>(environment.apiUri + '/api/Article/' + id);
  }

  public deleteArticle(id: string){
    return this.http.delete(environment.apiUri + '/api/Article/' + id);
  }

  public updateArticle(id: string, article: CreateArticleDto){
    return this.http.put(environment.apiUri + '/api/Article/' + id,  article);
  }
}
