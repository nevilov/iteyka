import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CreateMessageDto, MessageDto} from "../dtos/messageDto";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})

export class MessageService{
  private apiUri = environment.apiUri;
  private controllerRoute = '/api/message/';
  constructor(private http: HttpClient){

  }

  public sendMessage(message: CreateMessageDto){
    return this.http.post(this.apiUri + this.controllerRoute, message);
  }

  public getMessageByChatId(chatId: string): Observable<MessageDto[]>{
    return this.http.get<MessageDto[]>(this.apiUri + this.controllerRoute + chatId);
  }

}
