import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';


@Injectable()
export class TokenInterceptor implements HttpInterceptor{

  intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (localStorage.getItem('token') !== null){
      req = req.clone({
        headers: new HttpHeaders({
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }),
      });
    }
    return next.handle(req);
  }
}
