import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CategoryDto, CategoryList} from '../dtos/categoryDto';
import {environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class CategoryService{
  apiUri = environment.apiUri;
  controllerRoute = '/api/Category/';

  constructor(private httpClient: HttpClient) {
  }

  public getAll(): Observable<CategoryList>{
    return this.httpClient
      .get<CategoryList>(this.apiUri + this.controllerRoute);
  }

  public getBySlug(slug: string):Observable<CategoryDto>{
    return this.httpClient.get<CategoryDto>(this.apiUri + this.controllerRoute + slug)
  }
}
