import {Injectable} from '@angular/core';
import {HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CommonService{

  public configureParams(queryParams: any): HttpParams{
    let params = new HttpParams();

    Object.keys(queryParams)
      .sort()
      .forEach((key) => {
        const value = queryParams[key];
        if (value !== null && value !== '') {
          params = params.set(key, value.toString());
        }
      });

    return params;
  }
}
