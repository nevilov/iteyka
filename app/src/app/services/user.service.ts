import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {BlockUser, UpdateRole, UserDto} from '../dtos/userDto';
import {listOfItemsWithPaging} from "../dtos/commonDto";
import {CommonService} from "./common.service";

@Injectable({
  providedIn: 'root'
})

export class UserService{
  constructor(private http: HttpClient,
              private commonService: CommonService) {
  }

  public getUserByUsername(userName: string) : Observable<UserDto>{
    return this.http.get<UserDto>(environment.apiUri + '/api/User/' + userName);
  }

  public getCurrentUser(): Observable<UserDto>{
    return this.http.get<UserDto>(environment.apiUri + '/api/User/');
  }

  public getUsers(queryParams: any): Observable<listOfItemsWithPaging<UserDto>>{
    let params = this.commonService.configureParams(queryParams);

    return this.http.get<listOfItemsWithPaging<UserDto>>(environment.apiUri + '/api/User/get/all', {params});
  }

  public updateUser(user: UserDto){
    return this.http.put(environment.apiUri + '/api/User/', user);
  }

  public blockUser(dto: BlockUser){
    return this.http.post(environment.apiUri + '/api/User/block', dto);
  }

  public updateRole(dto: UpdateRole){
    return this.http.put(environment.apiUri + '/api/User/role', dto);
  }
}
