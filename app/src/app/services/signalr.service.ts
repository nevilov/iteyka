import {Injectable} from "@angular/core";
import * as signalR from '@microsoft/signalr';
import {SignalRMessage} from "../dtos/messageDto";
import {Subject} from "rxjs";
import {log} from "util";

@Injectable({
  providedIn: 'root'
})

export class SignalrService {
  private hubConnection: signalR.HubConnection;

  private _messagesSubject = new Subject<SignalRMessage>();

  public get messages$(){
    return this._messagesSubject.asObservable();
  }

  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:5001/signalr/chat', {
        accessTokenFactory: () => {
          var token = localStorage.getItem('token');
          token = 'Bearer ' + token;
          return token;
        }
      })
      .withAutomaticReconnect()
      .build();

    this.hubConnection.onclose(console.log);

    this.hubConnection.on('message', data => {
      this._messagesSubject.next(data as any);
    });

    this.hubConnection.start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ', err));
  }

  public on = (methodName: string, handler: (data: any) => void) => {
    this.hubConnection?.on(methodName, data => handler(data));
  }
}
