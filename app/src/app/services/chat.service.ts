import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ChatDto, ChatListItemDto, CreateChatDto} from "../dtos/chatDto";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})

export class ChatService{
  private apiUri = environment.apiUri;
  private controllerRoute = '/api/chat/';

  constructor(private http: HttpClient) {
  }

  public getUserChats(): Observable<ChatListItemDto[]>{
    return this.http.get<ChatListItemDto[]>(this.apiUri + this.controllerRoute + 'user');
  }

  //public getChat(id: string): Observable<ChatDto>{
   // return this.http.get<ChatDto>(this.apiUri + this.controllerRoute + id);
 // }

  public createChat(chat: CreateChatDto){
    return this.http.post(this.apiUri + this.controllerRoute, chat);
  }
}
