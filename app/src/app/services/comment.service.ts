import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CommentDto, CreateCommentDto, listOfCategories} from '../dtos/commentDto';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class CommentService {
  constructor(private http: HttpClient) {
  }

  public getComments(articleId: string): Observable<listOfCategories> {
    return this.http.get<listOfCategories>(environment.apiUri + '/api/Comment/' + articleId);
  }

  public addComment(articleId: string, request: CreateCommentDto){
    return this.http.post(environment.apiUri + '/api/Comment/' + articleId, request );
  }
}
