import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CreateSubscriptionDto, SubscriptionDto} from '../dtos/subscriptionDto';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class SubscriptionService{
  controllerRoute = '/api/Subscription/';
  apiUri = environment.apiUri;
  constructor(private httpClient: HttpClient){
  }


  public getSubscription(id: string): Observable<SubscriptionDto>{
    return this.httpClient.get<SubscriptionDto>(this.apiUri + this.controllerRoute + id);
  }

  public subscribe(subscription: CreateSubscriptionDto){
    return this.httpClient.post(this.apiUri + this.controllerRoute, subscription);
  }

  public unsubscribe(source: string){
    return this.httpClient.delete(this.apiUri + this.controllerRoute + 'source/' + source);
  }

  public getUserSubscriptionBySource(source: string): Observable<SubscriptionDto>{
    return this.httpClient.get<SubscriptionDto>(this.apiUri + this.controllerRoute + 'source/' + source);
  }

  public getSubscribersCount(source: string): Observable<number>{
    return this.httpClient.get<number>(this.apiUri + this.controllerRoute + 'source/' + source + '/count');
  }

}
