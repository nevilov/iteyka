import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {ImageDto} from "../dtos/imageDto";

@Injectable({
  providedIn: 'root'
})

export class  ImageService{
  private apiUri = environment.apiUri;
  private controllerRoute = '/api/Image/';
  constructor(private http: HttpClient) {
  }

  public Upload(source: string, image: FormData){
    return this.http.post(this.apiUri + this.controllerRoute + source, image);
  }

  public GetById(id: string): Observable<ImageDto>{
    return this.http.get<ImageDto>(this.apiUri + this.controllerRoute + id);
  }

  public GetArticleImages(articleId: string): Observable<ImageDto[]>{
    return this.http.get<ImageDto[]>(this.apiUri + this.controllerRoute + 'article/' + articleId);
  }

  public GetUserAvatar(userId: string): Observable<ImageDto>{
    return this.http.get<ImageDto>(this.apiUri + this.controllerRoute + 'user/' + userId)
  };

  public Delete(id: string){
    return this.http.delete(this.apiUri + this.controllerRoute + id);
  }

  public Update(id: string, image: FormData){
    return this.http.put(this.apiUri + this.controllerRoute + id, image);
  }
}
