export class ImageDto{
  id: string;
  name: string;
  blob: string;
}

export enum ImageType{
  Article,
  Category,
  Author
}
