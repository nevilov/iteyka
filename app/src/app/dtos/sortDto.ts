export interface SortDto{
  searchKeyword: string;
  limit: number;
  offset:number;
  minDate: Date;
  maxDate: Date;
}
