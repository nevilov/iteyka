export class SubscriptionDto{
  id: string;
  subscriberId: string;
  type: SubscriptionType;
  source: string;
}

export enum SubscriptionType{
  Author, Category
}

export class CreateSubscriptionDto{
  type: SubscriptionType;
  source: string;
}
