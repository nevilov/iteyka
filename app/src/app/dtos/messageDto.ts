export class MessageDto{
  id: string;
  content: string;
  senderId: string;
  createdDate: Date;
  recipientId: string;
}

export class CreateMessageDto{
  chatId: string;
  content: string;
  recipientId: string;
}

export class SignalRMessage{
  id: string;
  senderId: string;
  content: string;
  recipientId: string;
  createdDate: string;
}
