export interface CategoryDto{
  id: string;
  name: string;
  slug: string;
  CreatedDate: Date;
  image: string;
  ChildCategories: Array<CategoryDto>;
}

export interface CategoryList {
  categories: CategoryDto[];
}

export interface CategoryForArticleDto{
  id: string;
  name: string;
  slug: string;
}
