export interface UserDto{
  id: string;
  userName: string;
  name: string;
  lastName: string;
  roleName: string;
  createdDate: Date;
}


export interface LoginResponse{
  userId: string;
  token: string;
}

export interface BlockUser{
  userId: string;
  endDate: string;
}

export interface UpdateRole{
  userId: string;
  roleName: string;
}
