import {UserDto} from './userDto';

export class CommentDto{
  content: string;
  author: UserDto;
}

export interface listOfCategories{
  comments: CommentDto[];
}

export class CreateCommentDto{
  content: string;
}
