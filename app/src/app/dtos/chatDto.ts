import {UserDto} from "./userDto";

export class ChatDto{
  id: string;
  name: string;
  creator: UserDto;
  companion: UserDto;
  createdDate: Date;
}

export class ChatListItemDto{
  id: string;
  name: string;
  creator: UserDto;
  companion: UserDto;
  createdDate: Date;
}

export class CreateChatDto{
  companionId: string;
}
