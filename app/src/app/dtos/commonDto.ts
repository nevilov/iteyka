export interface listOfItemsWithPaging<T>{
  items: T[];
  total: number;
  limit: number;
  offset: number;
}

export const Admin = 'Admin';
export const User = 'User';
export const Moderator = 'Moderator';
