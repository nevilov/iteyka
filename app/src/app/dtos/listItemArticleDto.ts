import {SortDto} from './sortDto';
import {CategoryDto, CategoryForArticleDto} from './categoryDto';
import {UserDto} from './userDto';

export interface CreateArticleDto{
  title: string;
  content: string;
  categoryId: string;
}

export interface ListItemArticleDto {
  id: string;
  title: string;
  content: string;
  createdDate: Date;
  author: UserDto;
}

export interface ArticleDto extends ListItemArticleDto{
  category: CategoryForArticleDto;
}

export interface ArticleSortingRequestDto extends SortDto{
  categorySlug: string;
  isSortingByRating: boolean;
  isSortingByCommentsCount: boolean;
}


