import {Component, Input} from "@angular/core";
import {ArticleDto, ListItemArticleDto} from "../../../dtos/listItemArticleDto";
import {UserDto} from "../../../dtos/userDto";

@Component({
  selector: 'app-article',
  templateUrl: 'article.component.html',
  styleUrls: ['article.component.scss']
})

export class ArticleComponent{
  @Input() article: ListItemArticleDto;
  @Input() user: UserDto;
  @Input() queryParam;
}
