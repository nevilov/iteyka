import {Component, OnInit} from '@angular/core';
import {CategoryService} from '../../../services/category.service';
import {CategoryDto} from '../../../dtos/categoryDto';
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit{
  public categories: CategoryDto[] = [];

  constructor(private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    this.categoryService.getAll()
      .pipe(untilDestroyed(this))
      .subscribe((data) => {
      this.categories = data.categories;
    });
  }


}
