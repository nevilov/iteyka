import { Component, OnInit } from '@angular/core';
import {UserDto} from "../../../dtos/userDto";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {ImageService} from "../../../services/image.service";
import {ImageDto} from "../../../dtos/imageDto";

@UntilDestroy()
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public isAuthorized: boolean = false;
  private token: string;
  public user: UserDto;
  public isUserMenuClicked: boolean;
  public imageDto: ImageDto;

  constructor(private userService: UserService,
              private router: Router,
              private imageService: ImageService) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('token');
    if(this.token != null){
      this.userService.getCurrentUser()
        .pipe(untilDestroyed(this))
        .subscribe(data => {
        this.isAuthorized = true;
        this.user = data;
        this.loadAvatar();
      });
    }

  }


  onUserMenuClick() {
    this.isUserMenuClicked = !this.isUserMenuClicked;
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    return this.router.navigateByUrl('').then(() => {
      window.location.reload();
    });
  }

  navigateToProfile() {
    return this.router.navigateByUrl('u/' + this.user.userName).then(() => {
      window.location.reload();
    });
  }

  loadAvatar(){
    this.imageService
      .GetUserAvatar(this.user.id)
      .subscribe(avatar => {
        this.imageDto = avatar;
        this.imageDto.blob = 'data:image/jpeg;base64,' + avatar.blob;
        console.log(this.imageDto)
      });
  }
}
