import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { MenuComponent } from './components/menu/menu.component';
import {SidebarComponent} from './components/sidebar/sidebar.component';
import {FooterComponent} from './components/footer/footer.component';
import {DateFormatPipe} from '../services/pipes/date.pipe';
import {ArticleComponent} from "./components/article/article.component";
import {PagingComponent} from "./components/paging/paging.component";


@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
  ],
  declarations: [
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    DateFormatPipe,
    ArticleComponent,
    PagingComponent
  ],
  exports: [
    MenuComponent,
    SidebarComponent,
    FooterComponent,
    DateFormatPipe,
    ArticleComponent,
    PagingComponent
  ],
})
export class SharedModule {}
