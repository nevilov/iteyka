import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {CommonModule} from '@angular/common';
import {ArticlePageComponent} from './articlePage/articlePage.component';
import {FeedComponent} from './feed.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    RouterModule
  ],
  declarations: [
    FeedComponent,
    ArticlePageComponent,

  ],
  exports: [
    FeedComponent,
    ArticlePageComponent
  ],
})
export class FeedModule {
}
