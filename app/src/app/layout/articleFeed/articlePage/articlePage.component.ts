import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ListItemArticleDto} from '../../../dtos/listItemArticleDto';
import {ArticleService} from '../../../services/article.service';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {UserDto} from '../../../dtos/userDto';
import {UserService} from '../../../services/user.service';

@UntilDestroy()
@Component({
  selector: 'app-articlePage',
  templateUrl: './articlePage.component.html',
  styleUrls: ['./articlePage.component.scss']
})

export class ArticlePageComponent implements OnInit{
  public categorySlug: string;
  public user: UserDto;
  articles: ListItemArticleDto[] = [];
  total: number;
  queryParam =  {
    categorySlug: '',
    isSortingByCommentsCount: false,
    isSortingByRating: false,
    limit: 10,
    offset: 0,
    subscriberId: '',
    searchKeyword: ''
  };

  constructor(private route: ActivatedRoute,
              private articleService: ArticleService,
              private userService: UserService) {
  }

  ngOnInit(): void {
    this.queryParam.categorySlug = this.route.snapshot.queryParamMap.get('category');
    this.queryParam.limit = 10;
    this.queryParam.offset = 0;

    if (localStorage.getItem('token')){
      this.userService.getCurrentUser()
        .pipe(untilDestroyed(this))
        .subscribe(user => {
          this.user = user;
          this.queryParam.subscriberId = user.id;
          this.loadArticles();
        });
    }
    else{
      this.loadArticles();
    }
  }

  loadArticles(){
    this.articleService.getArticles(this.queryParam)
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        this.articles = data.items;
        this.total = data.total;
        console.log(data);
      });
  }

  onPageChange(offset: number) {
    this.queryParam = { ...this.queryParam, offset };
    this.loadArticles();
  }

}
