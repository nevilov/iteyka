import {Component, OnInit} from "@angular/core";
import {UserDto} from "../../../dtos/userDto";
import {UserService} from "../../../services/user.service";

@Component({
  selector: 'app-userCabinet',
  templateUrl: './userCabinet.component.html',
  styleUrls: ['userCabinet.component.scss']
})

export class UserCabinetComponent implements OnInit{
  public user:UserDto;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getCurrentUser()
      .subscribe(user => {
        this.user = user;
      })
  }

}
