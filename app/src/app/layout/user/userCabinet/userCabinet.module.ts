import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FeedModule} from "../../articleFeed/feed.module";
import {SharedModule} from "../../../shared/shared.module";
import {RouterModule} from "@angular/router";
import {ArticleModule} from "../../article/article.module";
import {ReactiveFormsModule} from "@angular/forms";
import {UserCabinetComponent} from "./userCabinet.component";
import {UpdateUserComponent} from "./updateUser/updateUser.component";
import {BlockUserComponent} from "./blockUser/block-user.component";
import {UpdateUserRoleComponent} from "./changeRole/update-user-role.component";

@NgModule({
  imports: [
    FeedModule,
    CommonModule,
    SharedModule,
    RouterModule,
    ArticleModule,
    ReactiveFormsModule,
  ],
  declarations: [
    UserCabinetComponent,
    UpdateUserComponent,
    BlockUserComponent,
    UpdateUserRoleComponent
  ],
  exports: [
    UserCabinetComponent,
    UpdateUserComponent,
    BlockUserComponent,
    UpdateUserRoleComponent
  ]
})

export class UserCabinetModule{}
