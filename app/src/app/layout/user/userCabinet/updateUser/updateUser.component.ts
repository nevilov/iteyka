import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../../services/user.service';
import {UserDto} from '../../../../dtos/userDto';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {FormControl, FormGroup} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {ImageService} from '../../../../services/image.service';
import {ImageDto} from '../../../../dtos/imageDto';

@UntilDestroy()
@Component({
  selector: 'app-updateUser',
  templateUrl: './updateUser.component.html',
  styleUrls: ['./updateUser.component.scss']
})

export class  UpdateUserComponent implements OnInit{
  public user: UserDto;
  editUserForm: FormGroup;
  constructor(private userService: UserService,
              private toastrService: ToastrService,
              private router: Router,
              private imageService: ImageService) {
    this.editUserForm = new FormGroup({
      name: new FormControl(''),
      lastName: new FormControl('')
    });
  }
  public imageDto: ImageDto;
  fileToUpload: File;



  ngOnInit(): void {
    this.userService.getCurrentUser()
      .pipe(untilDestroyed(this))
      .subscribe(user => {
        this.user = user;
        this.editUserForm.setValue({
          name: user.name,
          lastName: user.lastName
        });
        this.loadAvatar();
      });
  }

  onSaveClick(){
    this.userService.updateUser(this.editUserForm.value)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        return this.router.navigateByUrl('').then(() => {
          this.toastrService.success('Successfully updated!');
        });
      },
        (error) => {
          console.log(error);
      });
  }

  onFileChange(event) {
    this.fileToUpload = event.target.files[0];
    const formData = new FormData();
    formData.append('image', this.fileToUpload, this.fileToUpload.name);
    console.log(formData.get('image'));
    if (this.imageDto.name === 'default'){
      this.uploadImage(formData);
    }
    else{
      this.updateImage(this.imageDto.id, formData);
    }
  }

  loadAvatar(){
    this.imageService
      .GetUserAvatar(this.user.id)
      .subscribe(avatar => {
        this.imageDto = avatar;
        this.imageDto.blob = 'data:image/jpeg;base64,' + avatar.blob;
      });
  }

  uploadImage(file: FormData){
    this.imageService.Upload(this.user.id, file)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.loadAvatar();
      });
  }

  updateImage(id: string, file: FormData){
    console.log(file)
    this.imageService.Update(id, file)
      .pipe(untilDestroyed(this))
      .subscribe(() => this.loadAvatar());
  }
}
