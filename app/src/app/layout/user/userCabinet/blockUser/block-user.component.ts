import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {UserService} from '../../../../services/user.service';
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'block-user',
  templateUrl: './block-user.component.html',
  styleUrls: ['block-user.component.scss']
})

export class BlockUserComponent implements OnInit{
  public blockUserForm: FormGroup;

  constructor(private userService: UserService,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.blockUserForm = new FormGroup({
      userId: new FormControl(' '),
      endDate: new FormControl( ' ')
    });
  }

  onBlockClick(){
    this.userService.blockUser(this.blockUserForm.value)
      .subscribe(() => this.toastrService.success('User is blocked'));
  }

}
