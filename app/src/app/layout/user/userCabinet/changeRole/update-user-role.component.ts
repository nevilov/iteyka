import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {UserService} from "../../../../services/user.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-update-user-role',
  templateUrl: './update-user-role.component.html',
  styleUrls: ['./update-user-role.component.scss']
})

export class UpdateUserRoleComponent implements OnInit{
  public updateUserForm: FormGroup;

  constructor(private userService: UserService,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.updateUserForm = new FormGroup({
      userId: new FormControl(' '),
      roleName: new FormControl( ' ')
    });
  }

  onRoleUpdate(){
    this.userService.updateRole(this.updateUserForm.value)
      .subscribe(() => {
        this.toastrService.success('Role updated!');
      })
  }
}
