import {NgModule} from '@angular/core';
import {FeedModule} from '../articleFeed/feed.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {ArticleModule} from '../article/article.module';
import {ReactiveFormsModule} from '@angular/forms';
import {UserProfileComponent} from './userProfile/userProfile.component';
import {UsersPageComponent} from "./usersPage/usersPage.component";
import {UserProfileModule} from "./userProfile/userProfile.module";
import {UserCabinetComponent} from "./userCabinet/userCabinet.component";
import {ChatComponent} from "./chat/chat.component";

@NgModule({
  imports: [
    FeedModule,
    CommonModule,
    SharedModule,
    RouterModule,
    ArticleModule,
    ReactiveFormsModule,
    UserProfileModule
  ],
  declarations: [
    UsersPageComponent,
    ChatComponent
  ],
  exports: [
    UsersPageComponent,
    ChatComponent
  ]
})

export class UserModule{}
