import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../services/user.service';
import {UserDto} from '../../../dtos/userDto';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {SubscriptionService} from '../../../services/subscription.service';
import {CreateSubscriptionDto, SubscriptionDto, SubscriptionType} from '../../../dtos/subscriptionDto';
import {ListItemArticleDto} from "../../../dtos/listItemArticleDto";
import {ArticleService} from "../../../services/article.service";
import {ChatService} from "../../../services/chat.service";
import {ToastrService} from "ngx-toastr";
import {CreateChatDto} from "../../../dtos/chatDto";
import {ImageService} from "../../../services/image.service";
import {ImageDto} from "../../../dtos/imageDto";

@UntilDestroy()
@Component({
  selector: 'app-userProfile',
  templateUrl: './userProfile.component.html',
  styleUrls: ['./userProfile.component.scss']
})

export class UserProfileComponent implements OnInit{
  public user: UserDto;
  public userName: string;
  public subscription: SubscriptionDto;
  public isSubscribed: boolean;
  public subscribersCount: number;
  public imageDto: ImageDto;
  public articles: ListItemArticleDto[] = [];
  public currentUserId;
  queryParam =  {
    categorySlug: '',
    isSortingByCommentsCount: false,
    isSortingByRating: false,
    limit: 10,
    offset: 0,
    searchKeyword: '',
    userId: ''
  };

  constructor(private userService: UserService,
              private imageService: ImageService,
              private articleService: ArticleService,
              private route: ActivatedRoute,
              private subscriptionService: SubscriptionService,
              private chatService: ChatService,
              private router: Router,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('userName')))
      .subscribe((data) => (this.userName = data));

    this.userService.getUserByUsername(this.userName)
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        this.user = data;
        if(localStorage.getItem('token') !== null){
          this.subscriptionService.getUserSubscriptionBySource(this.user.id)
            .pipe(untilDestroyed(this))
            .subscribe(subscription => {
              this.subscription = subscription;
              this.isSubscribed = true;
            });
          this.currentUserId = localStorage.getItem('userId');
        }
        this.loadAvatar();
        this.queryParam.userId = data.id;
        this.articleService.getArticles(this.queryParam)
          .pipe(untilDestroyed(this))
          .subscribe(data => {
            this.articles = data.items;
      });

        this.subscriptionService.getSubscribersCount(this.user.id)
          .pipe(untilDestroyed(this))
          .subscribe(count => {
            this.subscribersCount = count;
          });
      });

  }

  subscribe(){
    const subscription = new CreateSubscriptionDto();
    subscription.source = this.user.id;
    subscription.type = SubscriptionType.Author;

    this.subscriptionService.subscribe(subscription)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.isSubscribed = true;
        ++this.subscribersCount;
      });
  }

  unsubscribe() {
    this.subscriptionService.unsubscribe(this.user.id)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.isSubscribed = false;
        --this.subscribersCount;
      });
  }

  onCreateChat() {
    let chat = new CreateChatDto();
    chat.companionId = this.user.id;
    this.chatService.createChat(chat)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        return this.router.navigateByUrl('').then(() => {
          this.toastrService.success('Chat was created!', 'Chat');
        });
      });
  }

  loadAvatar(){
    this.imageService
      .GetUserAvatar(this.user.id)
      .subscribe(avatar => {
        this.imageDto = avatar;
        this.imageDto.blob = 'data:image/jpeg;base64,' + avatar.blob;
        console.log(this.imageDto)
      });
  }
}
