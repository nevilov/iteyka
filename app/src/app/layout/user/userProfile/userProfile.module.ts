import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {FeedModule} from "../../articleFeed/feed.module";
import {SharedModule} from "../../../shared/shared.module";
import {ArticleModule} from "../../article/article.module";
import {UserProfileComponent} from "./userProfile.component";
import {UserArticlesComponent} from "./userArticles/userArticles.component";
import {UserCabinetModule} from "../userCabinet/userCabinet.module";

@NgModule({
  imports: [
    FeedModule,
    CommonModule,
    SharedModule,
    RouterModule,
    ArticleModule,
    ReactiveFormsModule,
    UserCabinetModule
  ],
  declarations: [
    UserProfileComponent,
    UserArticlesComponent
  ],
  exports: [
    UserProfileComponent,
    UserArticlesComponent
  ]
})

export class UserProfileModule{}
