import {Component, OnInit} from "@angular/core";
import {listOfItemsWithPaging} from "../../../dtos/commonDto";
import {UserDto} from "../../../dtos/userDto";
import {UserService} from "../../../services/user.service";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
  selector: 'app-userPage',
  templateUrl: './usersPage.component.html',
  styleUrls: ['./usersPage.component.scss']
})

export class UsersPageComponent implements OnInit{
  userList: listOfItemsWithPaging<UserDto>;

  constructor(private userService: UserService){}


  ngOnInit(): void {
    this.userService.getUsers('')
      .pipe(untilDestroyed(this))
      .subscribe(data => {
      this.userList = data;
      console.log(data);
    });
  }

}
