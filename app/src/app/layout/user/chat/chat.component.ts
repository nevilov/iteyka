import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ChatService} from '../../../services/chat.service';
import {MessageService} from '../../../services/message.service';
import {ChatDto, ChatListItemDto} from '../../../dtos/chatDto';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {CreateMessageDto, MessageDto, SignalRMessage} from '../../../dtos/messageDto';
import {UserDto} from "../../../dtos/userDto";
import {UserService} from "../../../services/user.service";
import {SignalrService} from "../../../services/signalr.service";

@UntilDestroy()
@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})

export class ChatComponent implements OnInit{
  @ViewChild('chat', { read: ElementRef }) Chat!: ElementRef;
  @ViewChild('messageText', { read: ElementRef }) MessageInput!: ElementRef;

  public chats: ChatListItemDto[] = [];
  public user: UserDto;
  public messages: MessageDto[] = [];
  public activeChatId: string;
  public currentRecipientId;

  constructor(private chatService: ChatService,
              private messageService: MessageService,
              private userService: UserService,
              private signalRService: SignalrService) {
  }

  ngOnInit(): void {
    this.signalRService.startConnection();

    this.signalRService.messages$.subscribe( (msg) => {
      this._handleMessage(msg);
    });

    this.chatService.getUserChats()
      .pipe(untilDestroyed(this))
      .subscribe(chats => {
      this.chats = chats;
    });

    this.userService
      .getCurrentUser()
      .pipe(untilDestroyed(this))
      .subscribe(user => {
        this.user = user;
      })

  }

  onSendMessage(value: string) {
    const message = new CreateMessageDto();
    message.content = value;
    message.chatId = this.activeChatId;
    message.recipientId = this.currentRecipientId;

    this.messageService.sendMessage(message)
      .pipe(untilDestroyed(this))
      .subscribe();

    this.Chat.nativeElement.scrollTo(0, this.Chat.nativeElement.scrollHeight);
    this.MessageInput.nativeElement.value = '';
  }

  onChatClick(chat: ChatListItemDto) {
    this.activeChatId = chat.id;
    if(chat.creator.id === this.user.id ){
      this.currentRecipientId = chat.companion.id;
    }
    else{
      this.currentRecipientId = chat.creator.id;
    }
    this.messageService.getMessageByChatId(chat.id)
      .pipe(untilDestroyed(this))
      .subscribe(messages => {
        this.messages = messages;
      });
  }

  private _handleMessage = (message: SignalRMessage) => {
    console.log(message);
    const chatMessage: MessageDto = {
      recipientId: message.recipientId,
      createdDate: new Date(),
      content: message.content,
      senderId: message.senderId,
      id: message.id
    };
    this.messages.push(chatMessage);
  };
}
