import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {ArticleService} from '../../../services/article.service';
import {CategoryService} from '../../../services/category.service';
import {CategoryDto} from '../../../dtos/categoryDto';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {ImageService} from '../../../services/image.service';
import {ToastrService} from 'ngx-toastr';
import {Route, Router} from '@angular/router';

@UntilDestroy()
@Component({
  selector: 'app-createArticle',
  templateUrl: './createArticle.component.html',
  styleUrls: ['./createArticle.component.scss']
})

export class CreateArticleComponent implements OnInit{
  filesToUpload: File[] = [];
  categories: CategoryDto[] = [];
  imagePreviews: any[] = [];
  articleId: string;

  constructor(private articleService: ArticleService,
              private categoryService: CategoryService,
              private imageService: ImageService,
              private toastrService: ToastrService,
              private router: Router) {
  }

  createArticleForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
    categoryId: new FormControl('')
  });

  onPublishClick(){
    // Refactor in future
    let formsData = new FormData();
    for (const file of this.filesToUpload){
      formsData.append('image', file, file.name);
    }

    this.articleService.createArticle(this.createArticleForm.value)
      .pipe(untilDestroyed(this))
      .subscribe((articleId) => {
        this.articleId = articleId;
        this.sendImages(formsData);
      });
  }

  ngOnInit(): void {
    this.categoryService.getAll()
      .pipe(untilDestroyed(this))
      .subscribe(data => {
      this.categories = data.categories;
    });
  }

  sendImages(formData: FormData){
    this.imageService
      .Upload(this.articleId, formData)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        return this.router.navigateByUrl('').then( () =>
          this.toastrService.success('Article successful added', 'Article')
        );
      });
  }

  newFile() {
    if (this.filesToUpload.length < 5) {
      this.filesToUpload.push({} as File);
    }
  }

  removeFile(i: number) {
    if (this.filesToUpload.length > 1) {
      if (i > -1) {
        this.filesToUpload.splice(i, 1);
        this.imagePreviews.splice(i, 1);
      }
    }
  }

  onFileChange(event: any, i: number) {
    var reader = new FileReader();
    if (event.target.files.length > 0) {
      this.filesToUpload[i] = event.target.files[0];
      reader.readAsDataURL(this.filesToUpload[i]);
      reader.onload = (_event) => {
        this.imagePreviews[i] = reader.result;
      }
    }
  }

}
