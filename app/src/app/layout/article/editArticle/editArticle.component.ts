import {Component, OnInit} from "@angular/core";
import {ArticleDto} from "../../../dtos/listItemArticleDto";
import {ActivatedRoute, Router} from "@angular/router";
import {ArticleService} from "../../../services/article.service";
import {UserService} from "../../../services/user.service";
import {switchMap} from "rxjs/operators";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {FormControl, FormGroup} from "@angular/forms";
import {CategoryService} from "../../../services/category.service";
import {CategoryDto} from "../../../dtos/categoryDto";
import {UserDto} from "../../../dtos/userDto";
import {ToastrService} from "ngx-toastr";
import {ImageService} from "../../../services/image.service";
import {ImageDto} from "../../../dtos/imageDto";

@UntilDestroy()
@Component({
  selector: 'app-editArticle',
  templateUrl: './editArticle.component.html',
  styleUrls: ['./editArticle.component.scss']
})

export class EditArticleComponent implements OnInit{
  id: string;
  public article: ArticleDto;
  public categories: CategoryDto[] = [];
  public currentUser: UserDto;
  filesToUpload: File[] = [];
  imagePreviews: any[] = [];
  hasFiles: boolean;

  editArticleForm = new FormGroup({
    title: new FormControl(''),
    content: new FormControl(''),
    categoryId: new FormControl('')
  });

  constructor(private route: ActivatedRoute,
              private router: Router,
              private articleService: ArticleService,
              private categoryService: CategoryService,
              private userService: UserService,
              private toastrService: ToastrService,
              private imageService: ImageService) {
  }

  ngOnInit(): void {
    if(localStorage.getItem('token') === null){
        this.router.navigateByUrl('').then(() => {
          this.toastrService.error('Not authorized!');
        });
    }

    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('id')))
      .subscribe((data) => {
        this.id = data;
      });

    if (this.id !== null){
      this.userService.getCurrentUser().subscribe(currentUser => {
        this.currentUser = currentUser;
      });
      this.loadArticle();
    }

    this.categoryService.getAll()
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        console.log(data.categories);
        this.categories = data.categories;
      });
  }

  loadArticle(){
    this.articleService.getArticleById(this.id)
      .pipe(untilDestroyed(this))
      .subscribe((data) => {
        this.article = data;
        this.editArticleForm.setValue({
          title: data.title,
          content: data.content,
          categoryId: data.category.id
        });
      });
  }

  sendImages(formData: FormData){
    this.imageService
      .Upload(this.article.id, formData)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        return this.router.navigateByUrl('').then( () =>
          this.toastrService.success('Article successful added', 'Article')
        );
      });
  }

  onSave(){
    this.articleService.updateArticle(this.article.id, this.editArticleForm.value).subscribe(() => {
      let formsData = new FormData();
      for (const file of this.filesToUpload){
        formsData.append('image', file, file.name);
      }

      this.sendImages(formsData);
      this.router.navigateByUrl('article/' + this.article.id).then(() => {
        this.toastrService.success('Article was updated!', 'Article');
      });
    });
  }


  newFile() {
    if (this.filesToUpload.length < 5) {
      this.filesToUpload.push({} as File);
    }
  }

  removeFile(i: number) {
    if (this.filesToUpload.length > 1) {
      if (i > -1) {
        this.filesToUpload.splice(i, 1);
        this.imagePreviews.splice(i, 1);
      }
    }
  }

  onFileChange(event: any, i: number) {
    this.hasFiles = true;
    var reader = new FileReader();
    if (event.target.files.length > 0) {
      this.filesToUpload[i] = event.target.files[0];
      reader.readAsDataURL(this.filesToUpload[i]);
      reader.onload = (_event) => {
        this.imagePreviews[i] = reader.result;
      }
    }
  }
}
