import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {switchMap} from 'rxjs/operators';
import {ArticleDto} from '../../../dtos/listItemArticleDto';
import {ArticleService} from '../../../services/article.service';
import {CommentService} from '../../../services/comment.service';
import {CommentDto} from '../../../dtos/commentDto';
import {UserService} from '../../../services/user.service';
import {UntilDestroy, untilDestroyed} from '@ngneat/until-destroy';
import {ImageService} from "../../../services/image.service";
import {ImageDto} from "../../../dtos/imageDto";
import {ToastrService} from "ngx-toastr";

@UntilDestroy()
@Component({
  selector: 'app-detailArticle',
  templateUrl: './detailArticle.component.html',
  styleUrls: ['./detailArticle.component.scss']
})

export class DetailArticleComponent implements OnInit{
  @ViewChild('commentContent', { read: ElementRef }) commentInput!: ElementRef;
   Id: string;
   public article: ArticleDto;
   public comments: CommentDto[];
   public images: ImageDto[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private articleService: ArticleService,
              private commentService: CommentService,
              private imageService: ImageService,
              private userService: UserService,
              private toastrService: ToastrService) {
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('id')))
      .subscribe((data) => {
        this.Id = data;
      });

    if (this.Id !== null){
      this.loadArticle();
    }
  }

  loadArticle(){
    this.articleService.getArticleById(this.Id)
      .pipe(untilDestroyed(this))
      .subscribe((data) => {
      this.article = data;
      this.loadComments();
      this.loadImages();
    });
  }

  loadComments(){
    this.commentService.getComments(this.Id).subscribe((data) => {
      this.comments = data.comments;
    });
  }

  addComment(content: string) {
    const comment = new CommentDto();
    comment.content = content;

    this.commentService.addComment(this.Id, comment)
      .pipe(untilDestroyed(this))
      .subscribe( () => {
        this.toastrService.success('Comment was added');
        this.comments.push(comment);
        this.commentInput.nativeElement.value = '';
    });
  }

  loadImages(){
    this.imageService.GetArticleImages(this.article.id).subscribe(data => {
      this.images = data;
    });
  }
}
