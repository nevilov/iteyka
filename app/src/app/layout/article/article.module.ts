import {NgModule} from '@angular/core';
import {FeedModule} from '../articleFeed/feed.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {CreateArticleComponent} from './createArticle/createArticle.component';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {DetailArticleComponent} from './detailArticle/detailArticle.component';
import {DateFormatPipe} from '../../services/pipes/date.pipe';
import {ArticleControlComponent} from "./articleControl/articleControl.component";
import {EditArticleComponent} from "./editArticle/editArticle.component";

@NgModule({
  imports: [
    FeedModule,
    CommonModule,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    CreateArticleComponent,
    DetailArticleComponent,
    ArticleControlComponent,
    EditArticleComponent
  ],
  exports: [
    CreateArticleComponent,
    DetailArticleComponent,
    ArticleControlComponent,
    EditArticleComponent
  ]
})

export class ArticleModule{}
