import {Component, OnInit} from "@angular/core";
import {ArticleService} from "../../../services/article.service";
import {ListItemArticleDto} from "../../../dtos/listItemArticleDto";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {UserService} from "../../../services/user.service";
import {UserDto} from "../../../dtos/userDto";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@UntilDestroy()
@Component({
  selector: 'app-articleControl',
  templateUrl: './articleControl.component.html',
  styleUrls: ['./articleControl.component.scss']
})

export class ArticleControlComponent implements OnInit{
  articles: ListItemArticleDto[] = [];
  currentUser: UserDto;
  public total: number;
  queryParam =  {
    userId: '',
    categorySlug: '',
    isSortingByCommentsCount: false,
    isSortingByRating: false,
    limit: 10,
    offset: 0,
    searchKeyword: ''
  };

  constructor(private articleService: ArticleService,
              private userService: UserService,
              private toastrService: ToastrService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.queryParam.limit = 10;
    this.queryParam.offset = 0;

    this.loadArticles();
  }


  onEditArticleClick(articleId: string) {
    return this.router.navigateByUrl('article/edit/' + articleId);
  }

  loadArticles(){
    this.userService.getCurrentUser().subscribe(user => {
      this.currentUser = user;
      this.queryParam.userId = user.id;
      this.articleService.getArticles(this.queryParam)
        .pipe(untilDestroyed(this))
        .subscribe(data => {
          this.total = data.total;
          this.articles = data.items;
        });
    });
  }

  onPageChange(offset: number) {
    this.queryParam = { ...this.queryParam, offset };
    this.loadArticles();
  }

  onDeleteArticleClick(article: ListItemArticleDto) {
    if(confirm("Are u sure?")){
      const index = this.articles.indexOf(article, 0);
      if(index > -1){
        this.articles.splice(index, 1);
        this.articleService.deleteArticle(article.id).subscribe(() => {
          this.toastrService.success('Article was deleted');
        })
      }
    }
  }
}
