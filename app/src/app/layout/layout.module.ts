import {NgModule} from '@angular/core';
import {FeedModule} from './articleFeed/feed.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {RouterModule} from '@angular/router';
import {ArticleModule} from './article/article.module';
import {AuthModule} from './auth/auth.module';
import {UserModule} from './user/user.module';
import {CategoryModule} from "./category/category.module";

@NgModule({
  imports: [
    FeedModule,
    CommonModule,
    SharedModule,
    RouterModule,
    ArticleModule,
    AuthModule,
    UserModule,
    CategoryModule
  ]
})
export class LayoutModule {}
