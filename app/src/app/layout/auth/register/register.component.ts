import {Component} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {ToastrService} from "ngx-toastr";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent{
  constructor(private authService: AuthService,
              private router: Router,
              private toastrService: ToastrService) {
  }

  registerForm = new FormGroup({
    name: new FormControl(''),
    lastName: new FormControl(''),
    userName: new FormControl(''),
    email: new FormControl(''),
    password: new FormControl(''),
    repeatedPassword: new FormControl('')
  });

  createProfile() {
    this.authService.register(this.registerForm.value)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
      return this.router.navigateByUrl('login');
      this.toastrService.success('Register was successful', 'Register');
    },(error) => {
      this.toastrService.error('t', 'Something was go wrong');
    });
  }
}
