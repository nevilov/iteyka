import {NgModule} from '@angular/core';
import {FeedModule} from '../articleFeed/feed.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {ArticleModule} from '../article/article.module';
import {LoginComponent} from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RegisterComponent} from './register/register.component';

@NgModule({
  imports: [
    FeedModule,
    CommonModule,
    SharedModule,
    RouterModule,
    ArticleModule,
    ReactiveFormsModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
  exports: [
    LoginComponent,
    RegisterComponent
  ]
})

export class AuthModule{}
