import {Component} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {AuthService} from '../../../services/auth.service';
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent{
  token: string;

  loginForm = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });

  constructor(private authService: AuthService,
              private router: Router,
              private toastrService: ToastrService) {
  }

  public onSendLoginData(){
    return this.authService.login(this.loginForm.value)
      .pipe(untilDestroyed(this))
      .subscribe(data => {
      console.log(data);
      return this.router.navigateByUrl('').then(() => {
        window.location.reload();
        this.toastrService.success('Login', 'Login was success!');
      });
    });
  }
}
