import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ArticleService} from "../../../services/article.service";
import {ListItemArticleDto} from "../../../dtos/listItemArticleDto";
import {UntilDestroy, untilDestroyed} from "@ngneat/until-destroy";
import {CreateSubscriptionDto, SubscriptionDto, SubscriptionType} from "../../../dtos/subscriptionDto";
import {SubscriptionService} from "../../../services/subscription.service";
import {switchMap} from "rxjs/operators";
import {CategoryDto} from "../../../dtos/categoryDto";
import {CategoryService} from "../../../services/category.service";

@UntilDestroy()
@Component({
  selector: 'app-detailCategory',
  templateUrl: './detailCategory.component.html',
  styleUrls: ['detailCategory.component.scss']
})

export class DetailCategoryComponent implements OnInit{
  public category: CategoryDto;
  public subscription: SubscriptionDto;
  public isSubscribed: boolean;
  public subscribersCount: number;
  public articles: ListItemArticleDto[] = [];
  public total: number;
  queryParam =  {
    categorySlug: '',
    isSortingByCommentsCount: false,
    isSortingByRating: false,
    limit: 10,
    offset: 0,
    searchKeyword: '',
    userId: ''
  };

  constructor(private route: ActivatedRoute,
              private articleService: ArticleService,
              private subscriptionService: SubscriptionService,
              private categoryService: CategoryService) {
  }

  ngOnInit(): void {
    this.route.paramMap
      .pipe(switchMap((params) => params.getAll('slug')))
      .subscribe((data) => {
        this.queryParam.categorySlug = data;
      });

    this.categoryService.getBySlug(this.queryParam.categorySlug)
      .pipe(untilDestroyed(this))
      .subscribe(category => {
        this.category = category;
        this.subscriptionService.getSubscribersCount(category.id)
          .pipe(untilDestroyed(this))
          .subscribe(count => this.subscribersCount = count);
      });

    this.loadArticles();
  }

  loadArticles(){
    this.articleService.getArticles(this.queryParam)
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        this.total = data.total;
        if (localStorage.getItem('token') !== null){
          this.subscriptionService.getUserSubscriptionBySource(this.category.id)
            .pipe(untilDestroyed(this))
            .subscribe(subscription => {
              this.subscription = subscription;
              this.isSubscribed = true;
            });
        }

        this.articles = data.items;
        console.log(data);
      });
  }

  subscribe(): void{
    const subscription = new CreateSubscriptionDto();
    subscription.source = this.category.id;
    subscription.type = SubscriptionType.Author;

    this.subscriptionService.subscribe(subscription)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.isSubscribed = true;
        ++this.subscribersCount;
      });
  }

  unsubscribe(): void {
    this.subscriptionService.unsubscribe(this.category.id)
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.isSubscribed = false;
        --this.subscribersCount;
      });
  }

  onPageChange(offset: number) {
    this.queryParam = { ...this.queryParam, offset };
    this.loadArticles();
  }
}
