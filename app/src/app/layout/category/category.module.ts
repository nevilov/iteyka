import {NgModule} from '@angular/core';
import {FeedModule} from '../articleFeed/feed.module';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../../shared/shared.module';
import {RouterModule} from '@angular/router';
import {ArticleModule} from '../article/article.module';
import {ReactiveFormsModule} from '@angular/forms';
import {UserProfileModule} from '../user/userProfile/userProfile.module';
import {DetailArticleComponent} from '../article/detailArticle/detailArticle.component';
import {DetailCategoryComponent} from "./detailCategory/detailCategory.component";

@NgModule({
  imports: [
    FeedModule,
    CommonModule,
    SharedModule,
    RouterModule,
    ArticleModule,
    ReactiveFormsModule,
    UserProfileModule
  ],
  declarations: [
    DetailCategoryComponent
  ],
  exports: [
    DetailCategoryComponent
  ]
})
export class CategoryModule {}
