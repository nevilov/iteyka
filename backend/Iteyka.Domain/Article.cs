﻿using System;
using System.Collections.Generic;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Article : BaseEntity
    {
        /// <summary>
        /// Headline article
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Content article
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Article comments
        /// </summary>
        public List<Comment> Comments { get; set; }

        /// <summary>
        /// Unique indefier category
        /// </summary>
        public Guid CategoryId { get; set; }

        /// <summary>
        /// Article category
        /// </summary>
        public Category Category { get; set; }

        /// <summary>
        /// Author
        /// </summary>
        public Profile Author { get; set; }

        /// <summary>
        /// Unique Indefier Author
        /// </summary>
        public Guid AuthorId { get; set; }

        /// <summary>
        /// Date when article is published
        /// </summary>
        public DateTime? PublishedDate { get; set; }
    }
}
