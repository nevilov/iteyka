using System;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Message : BaseEntity
    {
        public Guid ChatId { get; set; }

        public Chat Chat { get; set; }

        public Guid SenderId { get; set; }

        public Profile Sender { get; set; }

        public Guid RecipientId { get; set; }

        public Profile Recipient { get; set; }

        public string Content { get; set; }
    }
}
