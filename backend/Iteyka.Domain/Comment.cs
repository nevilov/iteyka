﻿using System;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Comment : BaseEntity
    {
        /// <summary>
        /// Main comment content
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Article to wich the comment was written
        /// </summary>
        public Article Article { get; set; }

        /// <summary>
        /// Unique indefier article
        /// </summary>
        public Guid ArticleId { get; set; }

        /// <summary>
        /// Person who wrote comment
        /// </summary>
        public Profile Author { get; set; }

        /// <summary>
        /// Unique indefier comment author 
        /// </summary>
        public Guid AuthorId { get; set; }
    }
}
