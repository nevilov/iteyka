﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Iteyka.Domain
{
    public class ApplicationUser : IdentityUser<Guid>
    {
    }
}
