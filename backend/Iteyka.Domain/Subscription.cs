﻿using System;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Subscription : BaseEntity
    {
        public Guid SubscriberId { get; set; }

        public Guid Source { get; set; }

        public SubscriptionType Type { get; set; }
    }

    public enum SubscriptionType
    {
        Author,
        Category
    }
}
