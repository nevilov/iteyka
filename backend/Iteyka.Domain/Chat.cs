using System;
using System.Collections;
using System.Collections.Generic;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Chat : BaseEntity
    {
        public string Name { get; set; }

        public Guid CreatorId { get; set; }

        public Profile Creator { get; set; }

        public Guid CompanionId { get; set; }

        public Profile Companion { get; set; }

        public ICollection<Message> Messages { get; set; }
    }
}
