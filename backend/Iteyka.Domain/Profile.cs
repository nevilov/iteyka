﻿using System.Collections.Generic;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Profile : BaseEntity
    {
        /// <summary>
        /// Name of profile
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// LastName of profile
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// List of user images
        /// </summary>
        public ICollection<Image> Images { get; set; }

        /// <summary>
        /// User status
        /// </summary>
        public string Status { get; set; }
    }
}
