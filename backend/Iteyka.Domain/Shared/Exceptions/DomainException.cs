﻿using System;

namespace Iteyka.Domain.Shared.Exceptions
{
    public class DomainException : ApplicationException
    {
        protected DomainException(string message) : base(message)
        {
        }
    }
}
