﻿using System;
namespace Iteyka.Domain.Shared
{
    public class BaseEntity
    {
        /// <summary>
        /// Unique identifier of the entity
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Date when entity created
        /// </summary>
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Date when entity updated
        /// </summary>
        public DateTime? UpdatedDate { get; set; }

        /// <summary>
        /// Date when entity removed
        /// </summary>
        public DateTime? RemovedDate { get; set; }

        /// <summary>
        /// Is entity deleted
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
