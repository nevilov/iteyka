using System;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Image : BaseEntity
    {
        public string Name { get; set; }
        public byte[] ImageBlob { get; set; }
        public Guid? Source { get; set; }
        public Guid UserId { get; set; }
        public Profile User { get; set; }
    }
}
