﻿using System;
using System.Collections.Generic;
using Iteyka.Domain.Shared;

namespace Iteyka.Domain
{
    public class Category : BaseEntity
    {
        /// <summary>
        /// Some category path
        /// </summary>
        public string Slug { get; set; }

        /// <summary>
        /// Name of category
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Parent category
        /// </summary>
        public Category ParentCategory { get; set; }

        /// <summary>
        /// Unique indefier parent category
        /// </summary>
        public Guid? ParentCategoryId { get; set; }

        /// <summary>
        /// List category childs
        /// </summary>
        public List<Category> ChildCategories { get; set; }
    }
}
