using FluentValidation;
using FluentValidation.AspNetCore;
using Iteyka.Api.Validators.Article;
using Iteyka.Api.Validators.User;
using Iteyka.Contracts.Dtos.Article;
using Iteyka.Contracts.Dtos.Identity;
using Iteyka.Contracts.Dtos.Profile;
using Microsoft.Extensions.DependencyInjection;

namespace Iteyka.Api.Extensions
{
    public static class FluentValidationModule
    {
        public static IServiceCollection AddFluentValidationModule(this IServiceCollection services)
        {
            services.AddMvc().AddFluentValidation();

            services
                .AddTransient<IValidator<ArticleRequestDto>, ArticleRequestDtoValidator>();

            services
                .AddTransient<IValidator<CreateProfileRequest>, RegisterRequestValidator>()
                .AddTransient<IValidator<LoginRequest>, LoginRequestValidator>();


            return services;
        }
    }
}
