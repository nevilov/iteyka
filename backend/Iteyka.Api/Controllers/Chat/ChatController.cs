﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Chat.Interfaces;
using Iteyka.Contracts.Dtos.Chat;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.Chat
{
    [ApiController]
    [Route("api/[controller]")]
    public class ChatController : ControllerBase
    {
        private readonly IChatService _chatService;

        public ChatController(IChatService chatService)
        {
            _chatService = chatService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ChatDto>> GetById(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _chatService.GetById(id, cancellationToken));
        }

        [HttpGet("user")]
        public async Task<ActionResult<IEnumerable<ChatListItemDto>>> GetUserChats(CancellationToken cancellationToken)
        {
            return Ok(await _chatService.GetUserChats(cancellationToken));
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateChatDto dto, CancellationToken cancellationToken)
        {
            await _chatService.CreateChat(dto, cancellationToken);
            return Ok();
        }
    }
}
