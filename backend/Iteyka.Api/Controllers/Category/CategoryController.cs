﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Category.Interfaces;
using Iteyka.Contracts.Dtos.Category;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.Category
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService) => _categoryService = categoryService;

        [HttpGet]
        public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
        {
            var result = await _categoryService.GetAllCategories(cancellationToken);
            return Ok(result);
        }

        [HttpGet("{slug}")]
        public async Task<ActionResult<CategoryDto>> GetBySlug(string slug, CancellationToken cancellationToken)
        {
            return Ok(await _categoryService.GetBySlug(slug, cancellationToken));
        }
    }
}
