using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Image.Interfaces;
using Iteyka.Contracts.Dtos.Image;
using Iteyka.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.Image
{
    [ApiController]
    [Route("api/[controller]")]
    public class ImageController : ControllerBase
    {
        private readonly IImageService _imageService;

        public ImageController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpPost("{source}")]
        public async Task<IActionResult> Upload(Guid source, IFormFile image, CancellationToken cancellationToken)
        {
            await _imageService.Upload(source, image, cancellationToken);
            return Ok();
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<ImageDto>> Get(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _imageService.GetById(id, cancellationToken));
        }

        [HttpGet("article/{articleId}")]
        public async Task<ActionResult<IEnumerable<ImageDto>>> GetArticleImages(Guid articleId,
            CancellationToken cancellationToken)
        {
            var images = await _imageService.GetArticleImages(articleId, cancellationToken);
            return Ok(images);
        }

        [HttpGet("user/{userId}")]
        public async Task<IActionResult> GetUserAvatar(Guid userId, CancellationToken cancellationToken)
        {
            var image = await _imageService.GetBySource(userId, cancellationToken);
            return Ok(image);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAvatar(Guid id, IFormFile image,  CancellationToken cancellationToken)
        {
            await _imageService.UpdateAvatar(id, image, cancellationToken);
            return Ok();
        }


        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id, CancellationToken cancellationToken)
        {
            await _imageService.Delete(id, cancellationToken);
            return Ok();
        }
    }
}
