﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Common;
using Iteyka.Application.Repositories;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.Services.Profile.Interfaces;
using Iteyka.Contracts.Common.Sort;
using Iteyka.Contracts.Dtos.Identity;
using Iteyka.Contracts.Dtos.Profile;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.User
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly IProfileService _profileService;
        private readonly IIdentityService _identityService;

        public UserController(
            IProfileService profileService,
            IIdentityService identityService
            )
        {
            _profileService = profileService;
            _identityService = identityService;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginRequest request, CancellationToken cancellationToken)
        {
            return Ok(await _identityService.Login(request, cancellationToken));
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(CreateProfileRequest request, CancellationToken cancellationToken)
        {
            return Ok(await _profileService.CreateUser(request, cancellationToken));
        }

        [HttpGet("{userName}")]
        public async Task<IActionResult> GetUserByUsername(string userName, CancellationToken cancellationToken)
        {
            return Ok(await _profileService.GetByUserName(userName, cancellationToken));
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> GetCurrentUser(CancellationToken cancellationToken)
        {
            return Ok(await _profileService.GetCurrentUser(cancellationToken));
        }

        [HttpGet("query")]
        public async Task<IActionResult> GetUsers([FromQuery] GetUsersRequest request, CancellationToken cancellationToken)
        {
            return Ok(await _profileService.GetUsers(request, cancellationToken));
        }

        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateUser(UserDto request, CancellationToken cancellationToken)
        {
            await _profileService.UpdateUser(request, cancellationToken);
            return Ok();
        }

        [HttpPut("role")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateRole(ChangeRoleDto dto, CancellationToken cancellationToken)
        {
            await _identityService.ChangeRole(dto, cancellationToken);
            return Ok();
        }

        [HttpPost("block")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Block([FromBody] BlockDto dto, CancellationToken cancellationToken)
        {
            await _identityService.Block(dto, cancellationToken);
            return Ok();
        }

        [HttpPatch("status")]
        [Authorize]
        public async Task<IActionResult> UpdateStatus(string stauts, CancellationToken cancellationToken)
        {
            await _profileService.UpdateStatus(stauts, cancellationToken);
            return Ok();
        }
    }
}
