using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Article.Interfaces;
using Iteyka.Contracts.Dtos.Article;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.Article
{
    [ApiController]
    [Route("api/[controller]")]
    public class ArticleController : ControllerBase
    {
        private readonly IArticleService _articleService;

        public ArticleController(IArticleService articleService)
        {
            _articleService = articleService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetArticleByIdResponseDto>> GetById(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _articleService.GetById(id, cancellationToken));
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create(ArticleRequestDto request, CancellationToken cancellationToken)
        {
            var result = await _articleService.Create(request, cancellationToken);
            return Ok(result);
        }

        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Update(Guid id, ArticleRequestDto request,
            CancellationToken cancellationToken)
        {
            await _articleService.Update(id, request, cancellationToken);
            return Ok();
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Remove(Guid id, CancellationToken cancellationToken)
        {
            await _articleService.Remove(id, cancellationToken);
            return Ok();
        }


        [HttpGet]
        public async Task<IActionResult> GetArticles([FromQuery] GetArticlesWithPagingRequestDto withPagingRequest, CancellationToken cancellationToken)
        {
            var result = await _articleService.Get(withPagingRequest, cancellationToken);

            return Ok(result);
        }

    }
}
