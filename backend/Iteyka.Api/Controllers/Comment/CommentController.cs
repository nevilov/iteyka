﻿    using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Comment.Interfaces;
using Iteyka.Contracts.Dtos.Comment;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.Comment
{
    [ApiController]
    [Route("api/[controller]")]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService _commentService;

        public CommentController(ICommentService commentService)
        {
            _commentService = commentService;
        }

        [HttpPost("{articleId}")]
        [Authorize]
        public async Task<IActionResult> Add(Guid articleId, CommentRequestDto request, CancellationToken cancellationToken)
        {
            await _commentService.Add(articleId, request, cancellationToken);
            return Ok();
        }

        [HttpGet("{articleId}")]
        public async Task<IActionResult> GetComments(Guid articleId, CancellationToken cancellationToken)
        {
            var result = await _commentService.Get(articleId, cancellationToken);
            return Ok(result);
        }
    }
}
