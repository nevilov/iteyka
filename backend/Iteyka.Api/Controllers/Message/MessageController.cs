﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Message.Interfaces;
using Iteyka.Contracts.Dtos.Message;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.Message
{
    [ApiController]
    [Route("api/[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
        }

        [HttpPost]
        public async Task<IActionResult> Send(CreateMessageDto dto, CancellationToken cancellationToken)
        {
            await _messageService.Send(dto, cancellationToken);
            return Ok();
        }

        [HttpGet("{chatId}")]
        public async Task<IActionResult> GetByChatId(Guid chatId, CancellationToken cancellationToken)
        {
            return Ok(await _messageService.GetByChatId(chatId, cancellationToken));
        }
    }
}
