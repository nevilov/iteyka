using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Subscription.Interfaces;
using Iteyka.Contracts.Dtos.Subscription;
using Microsoft.AspNetCore.Mvc;

namespace Iteyka.Api.Controllers.Subscription
{
    [ApiController]
    [Route("api/[controller]")]
    public class SubscriptionController : ControllerBase
    {
        private readonly ISubscriptionService _subscriptionService;

        public SubscriptionController(ISubscriptionService subscriptionService)
        {
            _subscriptionService = subscriptionService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<SubscriptionDto>> Get(Guid id, CancellationToken cancellationToken)
        {
            return Ok(await _subscriptionService.GetSubscription(id, cancellationToken));
        }

        [HttpPost]
        public async Task<IActionResult> Subscribe(CreateSubscriptionDto request, CancellationToken cancellationToken)
        {
            await _subscriptionService.Subscribe(request, cancellationToken);
            return Ok();
        }

        [HttpDelete("source/{source}")]
        public async Task<IActionResult> Unsubscribe(Guid source, CancellationToken cancellationToken)
        {
            await _subscriptionService.Unsubscribe(source, cancellationToken);
            return Ok();
        }

        [HttpGet("source/{source}")]
        public async Task<ActionResult<SubscriptionDto>> GetSubscriptionBySource(Guid source, CancellationToken cancellationToken)
        {
            return Ok(await _subscriptionService.GetUserSubscriptionBySource(source, cancellationToken));
        }

        [HttpGet("source/{source}/count")]
        public async Task<ActionResult<int>> GetSubscribersCount(Guid source, CancellationToken cancellationToken)
        {
            return Ok(await _subscriptionService.GetSubscribersCount(source, cancellationToken));
        }
    }
}
