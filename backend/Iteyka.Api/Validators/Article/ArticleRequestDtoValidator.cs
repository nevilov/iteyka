using FluentValidation;
using Iteyka.Application.Common;
using Iteyka.Contracts.Dtos.Article;

namespace Iteyka.Api.Validators.Article
{
    public class ArticleRequestDtoValidator : AbstractValidator<ArticleRequestDto>
    {
        public ArticleRequestDtoValidator()
        {
            RuleFor(x => x.Title)
                .MinimumLength(ValidationConstants.MinLength1)
                .WithMessage($"Min symbols to title is {ValidationConstants.MinLength1}")
                .MaximumLength(ValidationConstants.MaxLength100)
                .WithMessage($"Max symbols to title is {ValidationConstants.MaxLength100}");

            RuleFor(x => x.Content)
                .MinimumLength(ValidationConstants.MinLength10)
                .WithMessage($"Min symbols to content is {ValidationConstants.MinLength10}");

            RuleFor(x => x.CategoryId)
                .NotEmpty()
                .NotNull().WithMessage("CategoryId is can't be null");
        }
    }
}
