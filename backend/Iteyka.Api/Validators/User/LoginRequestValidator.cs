using FluentValidation;
using Iteyka.Application.Common;
using Iteyka.Contracts.Dtos.Identity;

namespace Iteyka.Api.Validators.User
{
    public class LoginRequestValidator: AbstractValidator<LoginRequest>
    {
        public LoginRequestValidator()
        {
            RuleFor(x => x.Email)
                .MaximumLength(ValidationConstants.MaxLength100);

            RuleFor(x => x.Password)
                .NotEmpty()
                .NotNull()
                .WithMessage("Password can't be empty");
        }
    }
}
