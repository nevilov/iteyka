using FluentValidation;
using Iteyka.Application.Common;
using Iteyka.Contracts.Dtos.Identity;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Api.Validators.User
{
    public class RegisterRequestValidator : AbstractValidator<CreateProfileRequest>
    {
        public RegisterRequestValidator()
        {
            RuleFor(x => x.Email)
                .EmailAddress().WithMessage("Write email!")
                .MaximumLength(ValidationConstants.MaxLength100).WithMessage($"Email can't be greater {ValidationConstants.MaxLength100} symbols");

            RuleFor(x => x.UserName)
                .NotEmpty().WithMessage("Username не может быть пустым")
                .MaximumLength(ValidationConstants.MaxLength100).WithMessage($"UserName can't be greater {ValidationConstants.MaxLength100} symbols");

            RuleFor(x => x.Password)
                .Matches("[A-Z]").WithMessage("Password must have capital letters")
                .Matches("[a-z]").WithMessage("Password must have lower case letters")
                .Matches("[0-9]").WithMessage("Password must have digit(s)")
                .MinimumLength(ValidationConstants.MinLength6)
                .WithMessage($"Password must be greater {ValidationConstants.MinLength6}");

            RuleFor(x => x.Name)
                .MinimumLength(ValidationConstants.MinLength1)
                .WithMessage($"Name must be greater {ValidationConstants.MinLength1} symbol")
                .MaximumLength(ValidationConstants.MaxLength100)
                .WithMessage($"Name can't be greater {ValidationConstants.MaxLength100}");

            RuleFor(x => x.LastName)
                .MinimumLength(ValidationConstants.MinLength1)
                .WithMessage($"Last name must be greater {ValidationConstants.MinLength1} symbol")
                .MaximumLength(ValidationConstants.MaxLength100)
                .WithMessage($"Last name can't be greater {ValidationConstants.MaxLength100}");
        }
    }
}
