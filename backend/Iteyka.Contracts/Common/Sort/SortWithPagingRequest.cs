using System;

namespace Iteyka.Contracts.Common.Sort
{
    public class SortWithPagingRequest : PagedRequest
    {
        public string SearchKeyword { get; set; }
        public DateTime MinDate { get; set; } //Date of publish
        public DateTime MaxDate { get; set; } = DateTime.UtcNow;

        //IT WILL
        //public bool IsSortingByAuthorRating { get; set; }

    }
}
