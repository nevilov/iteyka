using System.Collections.Generic;

namespace Iteyka.Contracts.Common.Sort
{
    public class PagedResponse<TEntity>
    {
        public IEnumerable<TEntity> Items { get; set; }
        public int Total { get; set; }
        public int Limit { get; set; }
        public int Offset { get; set; }
    }
}
