﻿using System;

namespace Iteyka.Contracts.Dtos.Profile
{
    public class UserDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public string UserName { get;set; }

        public string RoleName { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
