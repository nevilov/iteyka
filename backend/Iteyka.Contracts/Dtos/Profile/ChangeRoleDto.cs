﻿using System;

namespace Iteyka.Contracts.Dtos.Profile
{
    public class ChangeRoleDto
    {
        public Guid UserId { get; set; }

        public string RoleName { get; set; }
    }
}
