﻿using System;

namespace Iteyka.Contracts.Dtos.Profile
{
    public class BlockDto
    {
        public Guid UserId { get; set; }

        public DateTime EndDate { get; set; }
    }
}
