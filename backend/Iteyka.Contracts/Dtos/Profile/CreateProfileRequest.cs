﻿namespace Iteyka.Contracts.Dtos.Profile
{
    public class CreateProfileRequest
    {
        public string Name { get; set; }

        public string LastName { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public string RepeatedPassword { get; set; }
    }
}
