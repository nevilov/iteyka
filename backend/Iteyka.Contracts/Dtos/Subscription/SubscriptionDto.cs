﻿using System;
using Iteyka.Domain;

namespace Iteyka.Contracts.Dtos.Subscription
{
    public record SubscriptionDto
    {
        public Guid Id { get; set; }
        public Guid SubscriberId { get; set; }

        public Guid Source { get; set; }

        public SubscriptionType Type { get; set; }
    }
}
