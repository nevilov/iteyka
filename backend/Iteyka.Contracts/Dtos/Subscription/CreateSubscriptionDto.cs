﻿using System;
using Iteyka.Domain;

namespace Iteyka.Contracts.Dtos.Subscription
{
    public class CreateSubscriptionDto
    {
        public Guid Source { get; set; }

        public SubscriptionType Type { get; set; }
    }
}
