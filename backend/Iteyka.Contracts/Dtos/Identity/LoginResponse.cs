﻿using System;

namespace Iteyka.Contracts.Dtos.Identity
{
    public class LoginResponse
    {
        public Guid UserId { get; set; }

        public string Token { get; set; }
    }
}
