using System;
using System.Collections;
using System.Collections.Generic;
using Iteyka.Contracts.Dtos.Category;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Contracts.Dtos.Article
{
    public class GetArticleByIdResponseDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public CategoryResponse Category { get; set; }

        public UserDto Author { get; set; }
    }

    public class CategoryResponse
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }
    }
}
