﻿using System;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Contracts.Dtos.Article
{
    public class ArticleDto
    {
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime CreatedDate { get; set; }

        public UserDto Author { get; set; }
    }
}
