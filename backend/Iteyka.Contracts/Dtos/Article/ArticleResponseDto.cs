using System;

namespace Iteyka.Contracts.Dtos.Article
{
    public class ArticleResponseDto
    {
        public Guid Id { get; set; }
    }
}
