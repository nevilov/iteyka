using System;
using Iteyka.Contracts.Common.Sort;

namespace Iteyka.Contracts.Dtos.Article
{
    public class GetArticlesWithPagingRequestDto : SortWithPagingRequest
    {
        public Guid? UserId { get; set; }
        public Guid? SubscriberId { get; set; }
        public string CategorySlug { get; set; }

        public bool IsSortingByRating { get; set; }
        public bool IsSortingByCommentsCount { get; set; }
    }
}
