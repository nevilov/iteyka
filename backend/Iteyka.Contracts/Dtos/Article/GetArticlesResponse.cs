﻿using Iteyka.Contracts.Common.Sort;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Contracts.Dtos.Article
{
    public class GetArticlesResponse : PagedResponse<ArticleDto>
    {

    }
}
