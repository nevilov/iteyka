using System;

namespace Iteyka.Contracts.Dtos.Article
{
    public class ArticleRequestDto
    {
        public string Title { get; set; }

        public string Content { get; set; }

        public Guid CategoryId { get; set; }
    }
}
