using System;

namespace Iteyka.Contracts.Dtos.Image
{
    public class ImageDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public byte[] Blob { get; set; }
    }
}
