﻿using System;

namespace Iteyka.Contracts.Dtos.SignalR
{
    public class MessageDto
    {
        public string Id { get; set; }

        public string Content { get; set; }

        public string SenderId { get; set; }

        public string RecipientId { get; set; }

        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
    }
}
