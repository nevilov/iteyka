﻿using System.Collections.Generic;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Contracts.Dtos.Comment
{
    public class GetCommentsResponseDto
    {
        public List<CommentResponse> Comments { get; set; }

        public class CommentResponse
        {
            public string Content { get; set; }

            public UserDto Author { get; set; }
        }
    }

}
