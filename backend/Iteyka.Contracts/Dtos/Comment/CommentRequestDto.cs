﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Iteyka.Contracts.Dtos.Comment
{
    public class CommentRequestDto
    {
        [NotNull]
        [Required]
        public string Content { get; set; }
    }
}
