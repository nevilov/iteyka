using System;
using System.Collections;
using System.Collections.Generic;
using Iteyka.Contracts.Dtos.Message;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Contracts.Dtos.Chat
{
    public class ChatDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public UserDto Creator { get; set; }

        public UserDto Companion { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
