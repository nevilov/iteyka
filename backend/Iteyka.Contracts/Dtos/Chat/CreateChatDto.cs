﻿using System;

namespace Iteyka.Contracts.Dtos.Chat
{
    public class CreateChatDto
    {
        public Guid CompanionId { get; set; }
    }
}
