using System;
using System.Collections.Generic;

namespace Iteyka.Contracts.Dtos.Category
{
    public class CategoryDto
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }

        public DateTime CreatedDate { get; set; }

        public byte[] Image { get; set; }

        public IEnumerable<CategoryDto> ChildCategories { get; set; }
    }
}
