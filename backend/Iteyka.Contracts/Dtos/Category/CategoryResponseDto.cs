﻿using System;
using System.Collections.Generic;

namespace Iteyka.Contracts.Dtos.Category
{
    public class CategoryResponseDto
    {
        public IEnumerable<CategoryDto> Categories { get; set; }
    }
}
