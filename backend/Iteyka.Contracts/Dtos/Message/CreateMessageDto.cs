using System;

namespace Iteyka.Contracts.Dtos.Message
{
    public class CreateMessageDto
    {
        public string Content { get; set; }

        public Guid ChatId { get; set; }

        public Guid RecipientId { get; set; }
    }
}
