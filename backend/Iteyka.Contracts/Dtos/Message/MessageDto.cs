using System;

namespace Iteyka.Contracts.Dtos.Message
{
    public class MessageDto
    {
        public Guid Id { get; set; }

        public string Content { get; set; }

        public Guid SenderId { get; set; }

        public Guid RecipientId { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
