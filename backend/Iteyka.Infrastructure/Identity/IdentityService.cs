﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Common;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Contracts.Dtos.Identity;
using Iteyka.Contracts.Dtos.Profile;
using Iteyka.Domain;
using Iteyka.Domain.Shared.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace Iteyka.Infrastructure.Identity
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IConfiguration _configuration;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public IdentityService(
            UserManager<ApplicationUser> userManager
            , IHttpContextAccessor httpContextAccessor
            , IConfiguration configuration
            , RoleManager<ApplicationRole> roleManager
            , SignInManager<ApplicationUser> signInManager
            )
        {
            _userManager = userManager;
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _roleManager = roleManager;
            _signInManager = signInManager;
        }


        public Task<Guid> GetCurrentUserId(CancellationToken cancellationToken = default)
        {
            var claimsPrincipal = _httpContextAccessor.HttpContext?.User;
            var userId = _userManager.GetUserId(claimsPrincipal);
            return Task.FromResult(Guid.Parse(userId));
        }

        public async Task<Guid> CreateUser(RegisterRequest request, CancellationToken cancellationToken)
        {
            var user = new ApplicationUser
            {
                Email = request.Email,
                UserName = request.UserName,
            };

            var resultCreateUser = await _userManager.CreateAsync(user, request.Password);
            if (!resultCreateUser.Succeeded)
            {
                throw new NoRightsException("Something go wrong " + resultCreateUser.Errors.Select(a => a.Description));
            }

            await _userManager.AddToRoleAsync(user, RoleConstants.User);
            return user.Id;
        }

        public async Task<LoginResponse> Login(LoginRequest request, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByEmailAsync(request.Email);
            if(user == null)
            {
                throw new NotFoundException($"User with email {request.Email} was not found");
            }

            var signInResult = await _signInManager.CheckPasswordSignInAsync(user, request.Password, false);
            if (signInResult.IsLockedOut)
            {
                throw new NoRightsException($"User is blocked");
            }

            if (!signInResult.Succeeded)
            {
                throw new NoRightsException("Not rigth login or password");
            }

            var claims = new List<Claim>
             {
                 new Claim(ClaimTypes.Email, user.Email),
                 new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                 new(ClaimTypes.Name, user.UserName),
                 new(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
             };

            var userRoles = await _userManager.GetRolesAsync(user);
            claims.AddRange(userRoles.Select(role => new Claim(ClaimTypes.Role, role)));

            var token = new JwtSecurityToken
            (
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(15),
                issuer: _configuration["Jwt:ValidIssuer"],
                audience: _configuration["Jwt:ValidAudience"],
                notBefore: DateTime.UtcNow,
                signingCredentials: new SigningCredentials(
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SecretKey"])),
                    SecurityAlgorithms.HmacSha256
                )
            );

            return new LoginResponse
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                UserId = user.Id
            };
        }

        public async Task<ApplicationUser> FindUserByEmail(string email, CancellationToken cancellationToken)
        {
            return await _userManager.FindByEmailAsync(email);
        }

        public async Task<ApplicationUser> FindUserById(Guid id, CancellationToken cancellationToken)
        {
            return await _userManager.FindByIdAsync(id.ToString());
        }

        public async Task<ApplicationRole> GetUserRole(Guid userId, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            var roles = await _userManager.GetRolesAsync(user);
            var roleId = roles.FirstOrDefault();
            var role = await _roleManager.FindByNameAsync(roleId);

            return role;
        }

        public async Task<bool> IsInRole(Guid userId, string role, CancellationToken cancellationToken)
        {
            var identityUser = await _userManager.FindByIdAsync(userId.ToString());
            if (identityUser == null)
            {
                throw new NotFoundException("User was not found");
            }

            return await _userManager.IsInRoleAsync(identityUser, role);
        }

        public async Task ChangeRole(ChangeRoleDto dto, CancellationToken cancellationToken)
        {
            if(dto.RoleName == RoleConstants.Admin)
            {
                throw new NoRightsException("Can't asignee user to admin role");
            }
            var role = await _roleManager.FindByNameAsync(dto.RoleName);
            if(role is null)
            {
                throw new NotFoundException($"Role {dto.RoleName} was not found");
            }

            if(await IsInRole(dto.UserId, RoleConstants.Admin, cancellationToken))
            {
                throw new NoRightsException("Have no rights to change admin's role");
            }

            var user = await _userManager.FindByIdAsync(dto.UserId.ToString());

            if(user is null)
            {
                throw new NotFoundException($"User {dto.UserId} was not found");
            }
            var oldRole = await _userManager.GetRolesAsync(user);

            await _userManager.RemoveFromRoleAsync(user, oldRole.FirstOrDefault());
            await _userManager.AddToRoleAsync(user, dto.RoleName);
        }

        public async Task Block(BlockDto dto, CancellationToken cancellationToken)
        {
            var user = await _userManager.FindByIdAsync(dto.UserId.ToString());
            if (user is null)
            {
                throw new NotFoundException($"User {dto.UserId} was not found");
            }

            await _userManager.SetLockoutEnabledAsync(user, true);
            if (await IsInRole(dto.UserId, RoleConstants.Admin, cancellationToken))
            {
                throw new NoRightsException("Have no rights to block admin's role");
            }

            await _userManager.SetLockoutEndDateAsync(user, dto.EndDate);
        }
    }
}
