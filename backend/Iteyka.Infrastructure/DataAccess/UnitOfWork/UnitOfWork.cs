﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Repositories;
using Iteyka.Application.UnitOfWork;
using Iteyka.Domain.Shared.Exceptions;
using Iteyka.Infrastructure.DataAccess.Repositories;

namespace Iteyka.Infrastructure.DataAccess.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
        }

        #region Repositories
        private IArticleRepository _articleRepository;
        public IArticleRepository ArticleRepository
        {
            get
            {
                _articleRepository ??= new ArticleRepository(_context);
                return _articleRepository;
            }
        }

        private ICommentRepository _commentRepository;
        public ICommentRepository CommentRepository
        {
            get
            {
                _commentRepository ??= new CommentRepository(_context);
                return _commentRepository;
            }
        }

        private ICategoryRepository _categoryRepository;
        public ICategoryRepository CategoryRepository
        {
            get
            {
                _categoryRepository ??= new CategoryRepository(_context);
                return _categoryRepository;
            }
        }

        private IProfileRepository _profileRepository;
        public IProfileRepository ProfileRepository
        {
            get
            {
                _profileRepository ??= new ProfileRepository(_context);
                return _profileRepository;
            }
        }

        private ISubscriptionRepository _subscriptionRepository;
        public ISubscriptionRepository SubscriptionRepository
        {
            get
            {
                _subscriptionRepository ??= new SubscriptionRepository(_context);
                return _subscriptionRepository;
            }
        }

        private IImageRepository _imageRepository;
        public IImageRepository ImageRepository
        {
            get
            {
                _imageRepository ??= new ImageRepository(_context);
                return _imageRepository;
            }
        }

        private IChatRepository _chatRepository;
        public IChatRepository ChatRepository
        {
            get
            {
                _chatRepository ??= new ChatRepository(_context);
                return _chatRepository;
            }
        }

        private IMessageRepository _messageRepository;
        public IMessageRepository MessageRepository
        {
            get
            {
                _messageRepository ??= new MessageRepository(_context);
                return _messageRepository;
            }
        }

        #endregion

        #region Transaction controls
        public async Task SaveChanges(CancellationToken cancellationToken)
        {
            using (var dbContextTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    await _context.SaveChangesAsync(cancellationToken);
                    dbContextTransaction.Commit();
                }
                catch (Exception e)
                {
                    dbContextTransaction.Rollback();
                    throw new DuplicateException($"Something go wrong {e.Message}");
                }
            }
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
