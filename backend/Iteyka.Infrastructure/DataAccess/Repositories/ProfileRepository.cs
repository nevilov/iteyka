﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Repositories;
using Iteyka.Contracts.Dtos.Profile;
using Iteyka.Domain;
using Microsoft.EntityFrameworkCore;

namespace Iteyka.Infrastructure.DataAccess.Repositories
{
    public class ProfileRepository : IProfileRepository
    {
        private readonly ApplicationDbContext _context;

        public ProfileRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Create(Guid id, CreateProfileRequest request, CancellationToken cancellationToken)
        {
            await _context.Profiles.AddAsync(new Domain.Profile
            {
                Id = id,
                Name = request.Name,
                LastName = request.LastName,
                UserName = request.UserName
            }, cancellationToken);
        }

        public async Task<Domain.Profile> FindById(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Profiles
                .FirstOrDefaultAsync(a => a.Id == id, cancellationToken);
        }

        public async Task<Domain.Profile> FindByUserName(string userName, CancellationToken cancellationToken)
        {
            return await _context.Profiles
                .FirstOrDefaultAsync(a => a.UserName.Equals(userName), cancellationToken);
        }

        public async Task<IEnumerable<Domain.Profile>> FindUsers(GetUsersRequest request,
            CancellationToken cancellationToken)
        {
            var users = _context.Profiles.AsQueryable().AsNoTracking();
            if (!string.IsNullOrWhiteSpace(request.SearchKeyword))
            {
                users = users
                    .Where(u => u.Name.Contains(request.SearchKeyword)
                                || u.LastName.Contains(request.SearchKeyword)
                                || u.UserName.Contains(request.SearchKeyword));
            }

            return await users
                .Skip(request.Offset)
                .Take(request.Limit)
                .ToListAsync(cancellationToken);
        }
    }
}
