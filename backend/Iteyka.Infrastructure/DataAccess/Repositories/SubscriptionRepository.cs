﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Repositories;
using Iteyka.Domain;
using Microsoft.EntityFrameworkCore;

namespace Iteyka.Infrastructure.DataAccess.Repositories
{
    public class SubscriptionRepository : ISubscriptionRepository
    {
        private readonly ApplicationDbContext _context;

        public SubscriptionRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Subscribe(Subscription subscription, CancellationToken cancellationToken)
        {
            await _context.Subscribtions.AddAsync(subscription, cancellationToken);
        }

        public void Unsubscribe(Subscription subscription, CancellationToken cancellationToken)
        {
            _context.Subscribtions.Remove(subscription);
        }

        public async Task<Subscription> GetById(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Subscribtions.FirstOrDefaultAsync(a => a.Id == id, cancellationToken);
        }

        public async Task<Subscription> GetUserSubscriptionBySource(Guid source, Guid userId, CancellationToken cancellationToken)
        {
            return await _context
                .Subscribtions
                .FirstOrDefaultAsync(a => a.Source == source && a.SubscriberId == userId, cancellationToken);
        }

        public async Task<int> GetSubscribersCount(Guid source, CancellationToken cancellationToken)
        {
            return await _context
                .Subscribtions
                .Where(a => a.Source == source)
                .CountAsync(cancellationToken);
        }
    }
}
