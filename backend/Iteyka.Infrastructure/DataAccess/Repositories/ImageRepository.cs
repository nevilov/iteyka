using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Repositories;
using Iteyka.Domain;
using Microsoft.EntityFrameworkCore;

namespace Iteyka.Infrastructure.DataAccess.Repositories
{
    public class ImageRepository : IImageRepository
    {
        private readonly ApplicationDbContext _context;

        public ImageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Add(Image image, CancellationToken cancellationToken) =>
            await _context.AddAsync(image, cancellationToken);

        public async Task<Image> FindById(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Images.FirstOrDefaultAsync(i => i.Id == id, cancellationToken);
        }

        public async Task<IEnumerable<Image>> GetImages(Guid source, CancellationToken cancellationToken)
        {
            return await _context.Images
                .Where(i => i.Source == source)
                .ToListAsync(cancellationToken);
        }

        public void Delete(Image image, CancellationToken cancellationToken)
        {
            _context.Images.Remove(image);
        }
    }
}
