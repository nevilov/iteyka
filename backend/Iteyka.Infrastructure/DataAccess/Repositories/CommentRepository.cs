﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Repositories;
using Iteyka.Domain;
using Microsoft.EntityFrameworkCore;

namespace Iteyka.Infrastructure.DataAccess.Repositories
{
    public class CommentRepository : ICommentRepository
    {
        public ApplicationDbContext _context;

        public CommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task Add(Comment comment, CancellationToken cancellationToken)
        {
            await _context.Comments.AddAsync(comment, cancellationToken);
        }

        public async Task<IEnumerable<Comment>> FindComments(Guid articleId, CancellationToken cancellationToken)
        {
            return await _context.Comments
                .Where(c => c.ArticleId == articleId)
                .Include(a => a.Author)
                .ToListAsync(cancellationToken);
        }
    }
}
