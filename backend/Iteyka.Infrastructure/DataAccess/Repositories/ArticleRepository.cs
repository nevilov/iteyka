﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Iteyka.Application.Repositories;
using Iteyka.Contracts.Dtos.Article;
using Iteyka.Domain;

namespace Iteyka.Infrastructure.DataAccess.Repositories
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly ApplicationDbContext _context;

        public ArticleRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Article> FindById(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Articles
                .Include(c => c.Category)
                .Include(a => a.Author)
                .Where(a => !a.IsDeleted)
                .FirstOrDefaultAsync(a => a.Id == id, cancellationToken);
        }

        public async Task Add(Article entity, CancellationToken cancellationToken)
        {
            await _context.AddAsync(entity, cancellationToken);
        }

        public async Task<(IEnumerable<Domain.Article>, int)> FindArticles(GetArticlesWithPagingRequestDto request, CancellationToken cancellationToken)
        {
            var result = _context.Articles.AsQueryable();
            result = result
                .Where(a => !a.IsDeleted);

            if (request.SubscriberId.HasValue)
            {
                var subscriptions = await _context.Subscribtions
                    .Where(a => a.SubscriberId == request.SubscriberId)
                    .Select(a => a.Source)
                    .ToListAsync();

                result = result
                    .Where(a => subscriptions.Contains(a.AuthorId) ||
                                subscriptions.Contains(a.CategoryId));

                // result = result
                //     .Where(value => subscriptions
                //         .Where(a => a.Type == SubscriptionType.Category)
                //         .Select(a => a.Source)
                //         .Contains(value.CategoryId))
                //     .Where(value => subscriptions.Where(a => a.Type == SubscriptionType.Author)
                //         .Select(a => a.Source)
                //         .Contains(value.AuthorId));
            }

            else if(request.UserId.HasValue)
            {
                result = result
                    .Where(a => a.Author.Id == request.UserId);
            }

            if (request.CategorySlug != null)
            {
                result = result
                    .Where(c => c.Category.Slug.Equals(request.CategorySlug));
            }

            if (!string.IsNullOrEmpty(request.SearchKeyword))
            {
                result = result
                    .Where(a => a.Title.Contains(request.SearchKeyword));
            }

            result = result
                .Where(a => a.CreatedDate >= request.MinDate && a.CreatedDate <= request.MaxDate);

            //TODO WHERE ARTICLE REMOVED DATE !NULL
            //TODO WHERE ARTICLE IS ALREADY PUBLISHED

            if (request.IsSortingByRating)
            {
                result = result.OrderBy(a => a.Title);
            }
            else if (request.IsSortingByCommentsCount)
            {
                result = result.OrderBy(a => a.Comments.Count);
            }

            var total = result.Count();

            result = result
                .Include(a => a.Author)
                .Skip(request.Offset)
                .Take(request.Limit);

            return (await result.ToListAsync(cancellationToken), total);
        }
    }
}
