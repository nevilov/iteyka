using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Repositories;
using Iteyka.Domain;
using Microsoft.EntityFrameworkCore;

namespace Iteyka.Infrastructure.DataAccess.Repositories
{
    public class ChatRepository : IChatRepository
    {
        private readonly ApplicationDbContext _context;

        public ChatRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Chat> FindById(Guid id, CancellationToken cancellationToken)
        {
            return await _context.Chats
                .Include(a => a.Companion)
                .Include(a => a.Creator)
                .FirstOrDefaultAsync(i => i.Id == id, cancellationToken);
        }

        public async Task<IEnumerable<Chat>> FindUserChats(Guid userId, CancellationToken cancellationToken)
        {
            return await _context.Chats
                .Include(c => c.Creator)
                .Include(c => c.Companion)
                .Where(u => u.CompanionId == userId || u.CreatorId == userId)
                .ToListAsync(cancellationToken);
        }

        public async Task CreateChat(Chat chat, CancellationToken cancellationToken)
        {
            await _context.Chats.AddAsync(chat, cancellationToken);
        }
    }
}
