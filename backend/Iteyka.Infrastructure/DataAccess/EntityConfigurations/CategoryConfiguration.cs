﻿using System;
using System.Collections.Generic;
using Iteyka.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Iteyka.Infrastructure.DataAccess.EntityConfigurations
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Domain.Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.HasData(BaseCategory());
            builder.Property(c => c.ParentCategoryId).IsRequired(false);
        }

        public List<Domain.Category> BaseCategory()
        {
            return new List<Category>
            {
                new Category
                {
                    Id = Guid.Parse("12E70F6F-3899-4E16-B79E-192781B794B9"),
                    Name = "IT technologies",
                    Slug = "it-technologies",
                },

                new Category
                {
                    Id = Guid.Parse("64B2B5E7-06B4-4DE7-B35B-69D6C670C4BB"),
                    Name = "Developing",
                    Slug = "developing"
                },

                new Category
                {
                    Id = Guid.Parse("C9C4E06E-A241-4682-9764-7E4AFA12CB40"),
                    Name = "Science",
                    Slug = "science"
                },

                new Category
                {
                    Id = Guid.Parse("F302638E-EF16-4C51-9164-3206C37FBBF2"),
                    Name = "Stories",
                    Slug = "stories"
                },

                new Category
                {
                    Id = Guid.Parse("E8ADD61F-0FF9-42D4-AE3E-C58F8E4419A7"),
                    Name = "Design",
                    Slug = "design"
                },

                new Category
                {
                    Id = Guid.Parse("B3C3240A-D26F-4EE4-B7A0-60EEF5C2ADA7"),
                    Name = "User expirience",
                    Slug = "life"
                },

                new Category
                {
                    Id = Guid.Parse("BBE9005D-22DA-41A7-8437-CA7131C9038C"),
                    Name = "Seo",
                    Slug = "seo"
                }
            };
        }
    }
}
