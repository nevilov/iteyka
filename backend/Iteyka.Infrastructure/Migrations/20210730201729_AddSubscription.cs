﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Iteyka.Infrastructure.Migrations
{
    public partial class AddSubscription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Subscribtions",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SubscriberId = table.Column<Guid>(type: "uuid", nullable: false),
                    Source = table.Column<Guid>(type: "uuid", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true),
                    RemovedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscribtions", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "1586cf8b-065e-4cc9-ba30-0a760bca4f48");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "185d973d-5d35-4c3c-9d57-0b0e72993b22");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "67076edc-df2c-4d22-84e9-d0bc508ac743", "AQAAAAEAACcQAAAAEBtVSSbV+EE5yVnjIxzuMRzEkTUAXc7dP00cIEUuytSK3RBDjD92r+VCcmcHbnPARQ==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 888, DateTimeKind.Utc).AddTicks(293));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 888, DateTimeKind.Utc).AddTicks(1258));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 888, DateTimeKind.Utc).AddTicks(1281));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 888, DateTimeKind.Utc).AddTicks(1284));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 888, DateTimeKind.Utc).AddTicks(1270));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 888, DateTimeKind.Utc).AddTicks(1276));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 888, DateTimeKind.Utc).AddTicks(1273));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 30, 20, 17, 28, 904, DateTimeKind.Utc).AddTicks(1870));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Subscribtions");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "78e7310f-4428-4a8f-9f30-fde04a4ef3a4");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "b461466f-7b3a-4df0-9d32-80618aeb67d9");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "845dd1de-eef8-4d3f-b24b-97311c3b98fa", "AQAAAAEAACcQAAAAEJPHZ6L92Z7aFnYBL4oYWUSTICwm3EaOJpSCyYnErK0OeB5WWacYhrpp08tsWbn0HQ==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 312, DateTimeKind.Utc).AddTicks(1028));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 312, DateTimeKind.Utc).AddTicks(1930));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 312, DateTimeKind.Utc).AddTicks(1951));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 312, DateTimeKind.Utc).AddTicks(1954));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 312, DateTimeKind.Utc).AddTicks(1940));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 312, DateTimeKind.Utc).AddTicks(1946));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 312, DateTimeKind.Utc).AddTicks(1944));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 17, 19, 58, 18, 328, DateTimeKind.Utc).AddTicks(3547));
        }
    }
}
