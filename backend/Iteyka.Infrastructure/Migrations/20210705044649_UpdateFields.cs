﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Iteyka.Infrastructure.Migrations
{
    public partial class UpdateFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "AuthorId",
                table: "Comments",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "AuthorId",
                table: "Articles",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "d3770682-7b43-403a-9dfa-b6894a72a9f7");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "c26a5ea2-3c72-4bae-b91a-9c0f7ea6f06f");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "632c97a3-2e10-465e-96aa-29164931dd76", "AQAAAAEAACcQAAAAEIZDfEuXnMqJ8QtNTQye9LvUDJgmL7PhOqmI5RpM/gjwRTYTSg/2kdgTOyi6+0Av8w==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 696, DateTimeKind.Utc).AddTicks(4948));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 696, DateTimeKind.Utc).AddTicks(6148));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 696, DateTimeKind.Utc).AddTicks(6174));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 696, DateTimeKind.Utc).AddTicks(6177));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 696, DateTimeKind.Utc).AddTicks(6161));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 696, DateTimeKind.Utc).AddTicks(6167));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 696, DateTimeKind.Utc).AddTicks(6164));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 5, 4, 46, 48, 722, DateTimeKind.Utc).AddTicks(2385));

            migrationBuilder.CreateIndex(
                name: "IX_Comments_AuthorId",
                table: "Comments",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Articles_AuthorId",
                table: "Articles",
                column: "AuthorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Profiles_AuthorId",
                table: "Articles",
                column: "AuthorId",
                principalTable: "Profiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Comments_Profiles_AuthorId",
                table: "Comments",
                column: "AuthorId",
                principalTable: "Profiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Profiles_AuthorId",
                table: "Articles");

            migrationBuilder.DropForeignKey(
                name: "FK_Comments_Profiles_AuthorId",
                table: "Comments");

            migrationBuilder.DropIndex(
                name: "IX_Comments_AuthorId",
                table: "Comments");

            migrationBuilder.DropIndex(
                name: "IX_Articles_AuthorId",
                table: "Articles");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Comments");

            migrationBuilder.DropColumn(
                name: "AuthorId",
                table: "Articles");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "64d5e599-750a-4b70-bb5a-079d75c8c706");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "653fc443-578e-48fd-b96f-e4fb5647f634");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "20c50bcc-7de3-40c9-af03-0798048196f8", "AQAAAAEAACcQAAAAEG3gH3njFiZ+ZMZyvjUZ4hc01AIrdz3oRSqvk9sydy2XxQyykBREW0WtEHnqin29EA==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 586, DateTimeKind.Utc).AddTicks(1629));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 586, DateTimeKind.Utc).AddTicks(2547));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 586, DateTimeKind.Utc).AddTicks(2569));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 586, DateTimeKind.Utc).AddTicks(2572));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 586, DateTimeKind.Utc).AddTicks(2558));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 586, DateTimeKind.Utc).AddTicks(2564));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 586, DateTimeKind.Utc).AddTicks(2561));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 7, 4, 16, 25, 2, 602, DateTimeKind.Utc).AddTicks(4847));
        }
    }
}
