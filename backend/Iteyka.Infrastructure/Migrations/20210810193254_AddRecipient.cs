﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Iteyka.Infrastructure.Migrations
{
    public partial class AddRecipient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "RecipientId",
                table: "Messages",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "089ad8fe-8711-4f72-bec3-cff6b7b0d832");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "25d816e8-04ac-420b-b206-7e983a3d1e2e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "d44af877-88a1-49c5-8bac-119214fabe66", "AQAAAAEAACcQAAAAECje7xhgCs5AGO+42VIKD9yUMbEdK721cNTpWAsW0wceC6jiYrTHMV88BW4JEr2RUg==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(5621));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6549));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6572));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6574));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6560));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6567));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6564));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 687, DateTimeKind.Utc).AddTicks(7603));

            migrationBuilder.CreateIndex(
                name: "IX_Messages_RecipientId",
                table: "Messages",
                column: "RecipientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Messages_Profiles_RecipientId",
                table: "Messages",
                column: "RecipientId",
                principalTable: "Profiles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Messages_Profiles_RecipientId",
                table: "Messages");

            migrationBuilder.DropIndex(
                name: "IX_Messages_RecipientId",
                table: "Messages");

            migrationBuilder.DropColumn(
                name: "RecipientId",
                table: "Messages");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "7a9c55e1-705b-4bfb-9123-72addb0838e6");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "dddcbcd1-d6f6-459f-b922-0fed2aaabdac");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "897bbc0c-7c7d-4e64-ae4e-04ec7f56f419", "AQAAAAEAACcQAAAAEGTG8tSFpmbCWE2m5OQfSnY06WvXm71cxZDKeTiorw0qKT+tjC0xH50pMpS8rXXPaA==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 302, DateTimeKind.Utc).AddTicks(9567));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 303, DateTimeKind.Utc).AddTicks(505));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 303, DateTimeKind.Utc).AddTicks(562));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 303, DateTimeKind.Utc).AddTicks(565));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 303, DateTimeKind.Utc).AddTicks(516));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 303, DateTimeKind.Utc).AddTicks(521));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 303, DateTimeKind.Utc).AddTicks(519));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 9, 19, 30, 54, 327, DateTimeKind.Utc).AddTicks(4438));
        }
    }
}
