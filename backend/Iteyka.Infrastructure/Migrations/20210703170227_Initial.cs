﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Iteyka.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Slug = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    ParentCategoryId = table.Column<Guid>(type: "uuid", nullable: true),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RemovedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Categories_Categories_ParentCategoryId",
                        column: x => x.ParentCategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Articles",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Content = table.Column<string>(type: "text", nullable: true),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RemovedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Articles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Articles_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Content = table.Column<string>(type: "text", nullable: true),
                    ArticleId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    UpdatedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    RemovedDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Articles_ArticleId",
                        column: x => x.ArticleId,
                        principalTable: "Articles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Categories",
                columns: new[] { "Id", "CreatedDate", "Name", "ParentCategoryId", "RemovedDate", "Slug", "UpdatedDate" },
                values: new object[,]
                {
                    { new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"), new DateTime(2021, 7, 3, 17, 2, 26, 739, DateTimeKind.Utc).AddTicks(368), "IT technologies", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "it-technologies", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"), new DateTime(2021, 7, 3, 17, 2, 26, 739, DateTimeKind.Utc).AddTicks(1272), "Developing", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "developing", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"), new DateTime(2021, 7, 3, 17, 2, 26, 739, DateTimeKind.Utc).AddTicks(1283), "Science", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "science", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"), new DateTime(2021, 7, 3, 17, 2, 26, 739, DateTimeKind.Utc).AddTicks(1286), "Stories", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "stories", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"), new DateTime(2021, 7, 3, 17, 2, 26, 739, DateTimeKind.Utc).AddTicks(1289), "Design", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "design", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"), new DateTime(2021, 7, 3, 17, 2, 26, 739, DateTimeKind.Utc).AddTicks(1294), "User expirience", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "life", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"), new DateTime(2021, 7, 3, 17, 2, 26, 739, DateTimeKind.Utc).AddTicks(1297), "Seo", null, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "seo", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Articles_CategoryId",
                table: "Articles",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_ParentCategoryId",
                table: "Categories",
                column: "ParentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Comments_ArticleId",
                table: "Comments",
                column: "ArticleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Articles");

            migrationBuilder.DropTable(
                name: "Categories");
        }
    }
}
