﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Iteyka.Infrastructure.Migrations
{
    public partial class AddStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Status",
                table: "Profiles",
                type: "text",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "d5c56ab9-250b-4ff1-aa68-821387b32eaa");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "62c8d7cd-48bf-4826-9533-dd9f29d197e3");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "3868c6e3-f4de-4670-8b9c-48162e95cf66", "AQAAAAEAACcQAAAAEMywjD5DOdtRA7uqx4C6LjMB+G59PLLTkf0jX7EkDnPsAap8Gn7JMaVV2rnDDkW4bg==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 272, DateTimeKind.Utc).AddTicks(7824));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 272, DateTimeKind.Utc).AddTicks(8833));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 272, DateTimeKind.Utc).AddTicks(8855));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 272, DateTimeKind.Utc).AddTicks(8858));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 272, DateTimeKind.Utc).AddTicks(8845));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 272, DateTimeKind.Utc).AddTicks(8850));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 272, DateTimeKind.Utc).AddTicks(8848));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 13, 4, 35, 57, 298, DateTimeKind.Utc).AddTicks(1946));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Profiles");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("214ab4f1-b36b-4166-8f04-e59b7a998935"),
                column: "ConcurrencyStamp",
                value: "089ad8fe-8711-4f72-bec3-cff6b7b0d832");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: new Guid("2cc65a96-496d-4e44-bde1-90de9bfcf2d4"),
                column: "ConcurrencyStamp",
                value: "25d816e8-04ac-420b-b206-7e983a3d1e2e");

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                columns: new[] { "ConcurrencyStamp", "PasswordHash" },
                values: new object[] { "d44af877-88a1-49c5-8bac-119214fabe66", "AQAAAAEAACcQAAAAECje7xhgCs5AGO+42VIKD9yUMbEdK721cNTpWAsW0wceC6jiYrTHMV88BW4JEr2RUg==" });

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("12e70f6f-3899-4e16-b79e-192781b794b9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(5621));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("64b2b5e7-06b4-4de7-b35b-69d6c670c4bb"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6549));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("b3c3240a-d26f-4ee4-b7a0-60eef5c2ada7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6572));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("bbe9005d-22da-41a7-8437-ca7131c9038c"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6574));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("c9c4e06e-a241-4682-9764-7e4afa12cb40"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6560));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("e8add61f-0ff9-42d4-ae3e-c58f8e4419a7"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6567));

            migrationBuilder.UpdateData(
                table: "Categories",
                keyColumn: "Id",
                keyValue: new Guid("f302638e-ef16-4c51-9164-3206c37fbbf2"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 663, DateTimeKind.Utc).AddTicks(6564));

            migrationBuilder.UpdateData(
                table: "Profiles",
                keyColumn: "Id",
                keyValue: new Guid("38eea783-902f-41f3-aded-8c27dee079e9"),
                column: "CreatedDate",
                value: new DateTime(2021, 8, 10, 19, 32, 53, 687, DateTimeKind.Utc).AddTicks(7603));
        }
    }
}
