﻿using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.SignalR;
using Iteyka.Contracts.Dtos.SignalR;
using Iteyka.Infrastructure.SignalR.Hubs;
using Microsoft.AspNetCore.SignalR;

namespace Iteyka.Infrastructure.SignalR
{
    public class SignalRService : ISignalRService
    {
        private readonly IHubContext<ChatHub> _hubContext;

        public SignalRService(IHubContext<ChatHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task Send(MessageDto message, CancellationToken cancellationToken)
        {
            await _hubContext.Clients.Users(message.SenderId, message.RecipientId)
                .SendAsync("message", new
                {
                    message.Id,
                    message.SenderId,
                    message.RecipientId,
                    message.Content,
                    message.CreatedDate
                }, cancellationToken);
        }
    }
}
