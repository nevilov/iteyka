﻿using Iteyka.Application.SignalR;
using Iteyka.Infrastructure.SignalR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;

namespace Iteyka.Infrastructure.Extensions
{
    public static class SignlRModule
    {
        public static IServiceCollection AddSingalRModule(this IServiceCollection services)
        {
            services.AddSignalR();
            services
                .AddSingleton<IUserIdProvider, UserIdProvider>()
                .AddScoped<ISignalRService, SignalRService>();


            return services;
        }
    }
}
