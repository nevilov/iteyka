using Iteyka.Application.Services.Article.Implementations;
using Iteyka.Application.Services.Article.Interfaces;
using Iteyka.Application.Services.Category.Implementations;
using Iteyka.Application.Services.Category.Interfaces;
using Iteyka.Application.Services.Chat.Implementations;
using Iteyka.Application.Services.Chat.Interfaces;
using Iteyka.Application.Services.Comment.Implementations;
using Iteyka.Application.Services.Comment.Interfaces;
using Iteyka.Application.Services.Image.Implementation;
using Iteyka.Application.Services.Image.Interfaces;
using Iteyka.Application.Services.Message.Implementations;
using Iteyka.Application.Services.Message.Interfaces;
using Iteyka.Application.Services.Profile.Implementations;
using Iteyka.Application.Services.Profile.Interfaces;
using Iteyka.Application.Services.Subscription.Implementations;
using Iteyka.Application.Services.Subscription.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Iteyka.Infrastructure.Extensions
{
    public static class ApplicationModule
    {
        public static IServiceCollection AddApplicationModule(this IServiceCollection services)
        {
            services
                .AddScoped<IArticleService, ArticleService>()
                .AddScoped<ICommentService, CommentService>()
                .AddScoped<ICategoryService, CategoryService>()
                .AddScoped<IProfileService, ProfileService>()
                .AddScoped<ISubscriptionService, SubscriptionService>()
                .AddScoped<IImageService, ImageService>()
                .AddScoped<IChatService, ChatService>()
                .AddScoped<IMessageService, MessageService>();

            return services;
        }
    }
}
