﻿using Iteyka.Application.Repositories;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Infrastructure.DataAccess.Repositories;
using Iteyka.Infrastructure.DataAccess.UnitOfWork;
using Iteyka.Infrastructure.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ApplicationDbContext = Iteyka.Infrastructure.DataAccess.ApplicationDbContext;

namespace Iteyka.Infrastructure.Extensions
{
    public static class DataAccessModule
    {
        public static IServiceCollection AddDataAccessModule(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(opt =>
            {
                opt.UseNpgsql(configuration.GetConnectionString("PostgresDb"));
            });

            services.AddScoped<IIdentityService, IdentityService>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
