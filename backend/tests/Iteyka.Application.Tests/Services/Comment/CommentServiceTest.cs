using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Iteyka.Application.Services.Comment.Implementations;
using Iteyka.Application.Services.Comment.Interfaces;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Comment;
using Moq;
using Xunit;

namespace Iteyka.Application.Tests.Services.Comment
{
    public class CommentServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWork;
        private readonly ICommentService _commentService;
        private readonly Mock<IIdentityService> _identityServiceMock;

        public CommentServiceTest()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _identityServiceMock = new Mock<IIdentityService>();
            _commentService = new CommentService(_unitOfWork.Object, _identityServiceMock.Object);
        }

        [Theory]
        [AutoData]
        public async Task AddComment_Response_Success(Guid articleId, CommentRequestDto request, CancellationToken cancellationToken)
        {
            var articleToReturn = new Domain.Article()
            {
                Id = articleId,
                Title = "Some Test",
                Content = "Another test",
                CategoryId = Guid.NewGuid(),
                Author = new Domain.Profile()
                {
                    Id = Guid.NewGuid(),
                    Name = "test",
                    LastName = "test"
                }
            };
            _unitOfWork
                .Setup(a => a.ArticleRepository.FindById(articleId, cancellationToken))
                .ReturnsAsync(articleToReturn);
            _unitOfWork
                .Setup(c => c.CommentRepository.Add(It.IsAny<Domain.Comment>(),
                    cancellationToken));

            _identityServiceMock.Setup(a => a.GetCurrentUserId(cancellationToken)).ReturnsAsync(Guid.NewGuid());
            _unitOfWork.Setup(a => a.ProfileRepository.FindById(It.IsAny<Guid>(), cancellationToken)).ReturnsAsync(new Domain.Profile());

            await _commentService.Add(articleId,request, cancellationToken);

            _unitOfWork
                .Verify(a => a.ArticleRepository
                    .FindById(articleId, cancellationToken), Times.Once);
            _unitOfWork
                .Verify(c => c.CommentRepository
                    .Add(It.IsAny<Domain.Comment>(), cancellationToken), Times.Once);
            _unitOfWork
                .Verify(a => a.ProfileRepository
                    .FindById(It.IsAny<Guid>(), cancellationToken), Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task GetComments_Response_Success(Guid articleId, CancellationToken cancellationToken)
        {
            var articleToReturn = new Domain.Article()
            {
                Id = articleId,
                Title = "Some Test",
                Content = "Another test",
                CategoryId = Guid.NewGuid()
            };
            IEnumerable<Domain.Comment> commentsToReturn = new List<Domain.Comment>
            {
                new Domain.Comment
                {
                    Id = Guid.NewGuid(),
                    ArticleId = articleId,
                    Content = "Test",
                    Author = new Domain.Profile
                    {
                        Id = Guid.NewGuid(),
                        Name = "Test",
                        LastName = "test"
                    }
                }
            };

            _unitOfWork
                .Setup(a => a.ArticleRepository.FindById(articleId, cancellationToken))
                .ReturnsAsync(articleToReturn);

            _unitOfWork
                .Setup(c => c.CommentRepository.FindComments(articleId, cancellationToken))
                .ReturnsAsync(commentsToReturn);

            var result = await _commentService.Get(articleId, cancellationToken);

            Assert.NotNull(result);
            _unitOfWork
                .Verify(a => a.ArticleRepository.FindById(articleId, cancellationToken), Times.Once);
            _unitOfWork
                .Verify(c => c.CommentRepository.FindComments(articleId, cancellationToken), Times.Once);
        }
    }
}
