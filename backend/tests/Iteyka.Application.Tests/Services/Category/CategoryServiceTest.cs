using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Iteyka.Application.Services.Category.Implementations;
using Iteyka.Application.UnitOfWork;
using Moq;
using Xunit;

namespace Iteyka.Application.Tests.Services.Category
{
    public class CategoryServiceTest
    {
        private readonly Mock<IUnitOfWork> _unitOfWorkMock;

        private readonly CategoryService _categoryService;

        public CategoryServiceTest()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _categoryService = new CategoryService(_unitOfWorkMock.Object);
        }

        [Theory]
        [AutoData]
        public async Task GetAllCategory_Response_Success(CancellationToken cancellationToken)
        {
            _unitOfWorkMock
                .Setup(a => a.CategoryRepository
                    .GetAll(cancellationToken))
                .ReturnsAsync(new List<Domain.Category>
                {
                    new Domain.Category() {Id = Guid.NewGuid(), Slug = "Ya pomnu", CreatedDate = DateTime.UtcNow},
                    new Domain.Category() {Id = Guid.NewGuid(), Slug = "Chudnoe mgnovenie", CreatedDate = DateTime.UtcNow},
                    new Domain.Category() {Id = Guid.NewGuid(), Slug = "Peredo mnoy yavilas ti", CreatedDate =  DateTime.UtcNow}
                });

            var result = await _categoryService.GetAllCategories(cancellationToken);

            Assert.NotNull(result);
            _unitOfWorkMock.Verify(a => a.CategoryRepository.GetAll(cancellationToken), Times.Once);
        }
    }
}
