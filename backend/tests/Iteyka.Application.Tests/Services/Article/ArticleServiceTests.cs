﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using Iteyka.Application.Services.Article.Implementations;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Article;
using Iteyka.Domain.Shared.Exceptions;
using Moq;
using Xunit;

namespace Iteyka.Application.Tests.Services.Article
{
    public class ArticleServiceTests
    {
        private Mock<IUnitOfWork> _unitOfWorkMock;
        private Mock<IIdentityService> _identityService;
        private readonly ArticleService _articleService;

        public ArticleServiceTests()
        {
            _unitOfWorkMock = new Mock<IUnitOfWork>();
            _identityService = new Mock<IIdentityService>();
            _articleService = new ArticleService(_unitOfWorkMock.Object, _identityService.Object);
        }

        [Theory]
        [AutoData]
        public async Task CreateArticke_Response_Success(ArticleRequestDto request
            , CancellationToken cancellationToken)
        {
            _unitOfWorkMock
                .Setup(a => a.ArticleRepository
                .Add(It.IsAny<Domain.Article>(), It.IsAny<CancellationToken>()));
            _identityService
                .Setup(a => a.GetCurrentUserId(cancellationToken)).ReturnsAsync(Guid.NewGuid());
            _unitOfWorkMock
                .Setup(a => a.ProfileRepository.FindById(It.IsAny<Guid>(), cancellationToken)).ReturnsAsync(new Domain.Profile());

            var result = await _articleService.Create(request, cancellationToken);

            Assert.NotNull(result);
            _unitOfWorkMock
                .Verify(a => a.ArticleRepository
                .Add(It.IsAny<Domain.Article>(), It.IsAny<CancellationToken>()), Times.Once);
            _unitOfWorkMock
                .Verify(a => a.ProfileRepository.FindById(It.IsAny<Guid>(), cancellationToken), Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task GetArticle_Response_Success(Guid id
            , CancellationToken cancellationToken)
        {
            var findedArticle = new Domain.Article
            {
                Id = id,
                Title = "Som hi",
                Content = "Js otstoy",
                CategoryId = Guid.Parse("E354F401-72C0-4A81-950A-D2C950CF1193"),
                Category = new Domain.Category
                {
                    Id = Guid.Parse("E354F401-72C0-4A81-950A-D2C950CF1193"),
                    Name = "Test",
                    Slug = "Test"
                },
                Author = new Domain.Profile()
            };


           _unitOfWorkMock
                .Setup(a => a.ArticleRepository.FindById(id, cancellationToken))
                .ReturnsAsync(findedArticle);

            var result = await _articleService.GetById(id, cancellationToken);

            Assert.NotNull(result);
            Assert.Equal(findedArticle.Title, result.Title);
            Assert.Equal(findedArticle.Content, result.Content);
            _unitOfWorkMock
                .Verify(a => a.ArticleRepository.FindById(id, cancellationToken), Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task UpdateArticle_Response_NotFoundException(Guid id
            , ArticleRequestDto request
            , CancellationToken cancellationToken)
        {
            _unitOfWorkMock
                .Setup(a => a.ArticleRepository.FindById(id, cancellationToken))
                .ReturnsAsync((Domain.Article)null);


            var result = await Assert.ThrowsAsync<NotFoundException>(async () =>
            {
                await _articleService.Update(id, request, cancellationToken);
            });
            _unitOfWorkMock
                .Verify(a => a.ArticleRepository.FindById(id, cancellationToken), Times.Once);
        }

        [Theory]
        [AutoData]
        public async Task GetArticles_Response_Success(GetArticlesWithPagingRequestDto withPagingRequest, CancellationToken cancellationToken)
        {
            IEnumerable<Domain.Article> articlesToReturn = new List<Domain.Article>
            {
                new Domain.Article
                {
                    Id = Guid.NewGuid(),
                    Title = "Test",
                    Content = "Test",
                    CategoryId = Guid.NewGuid()
                },
                new Domain.Article
                {
                    Id = Guid.NewGuid(),
                    Title = "Test",
                    Content = "Test",
                    CategoryId = Guid.NewGuid()
                },
                new Domain.Article
                {
                    Id = Guid.NewGuid(),
                    Title = "Test",
                    Content = "Test",
                    CategoryId = Guid.NewGuid()
                },
            };

            _unitOfWorkMock
                .Setup(a => a.ArticleRepository.FindArticles(withPagingRequest, cancellationToken))
                .ReturnsAsync((articlesToReturn, 5));

            var result = _articleService.Get(withPagingRequest, cancellationToken);

            Assert.NotNull(result);
            _unitOfWorkMock
                .Verify(a => a.ArticleRepository
                .FindArticles(withPagingRequest, cancellationToken), Times.Once);
        }
    }
}
