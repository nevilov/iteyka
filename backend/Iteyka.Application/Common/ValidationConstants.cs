namespace Iteyka.Application.Common
{
    public static class ValidationConstants
    {
        public static int MaxLength10 = 10;
        public static int MaxLength100 = 100;
        public static int MaxLength10000 = 10000;

        public static int MinLength1 = 1;
        public static int MinLength6 = 6;
        public static int MinLength10 = 10;
    }
}
