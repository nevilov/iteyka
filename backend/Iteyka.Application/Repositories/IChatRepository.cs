using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Domain;

namespace Iteyka.Application.Repositories
{
    public interface IChatRepository
    {
        Task<Chat> FindById(Guid id, CancellationToken cancellationToken);

        Task<IEnumerable<Chat>> FindUserChats(Guid userId, CancellationToken cancellationToken);

        Task CreateChat(Chat chat, CancellationToken cancellationToken);

    }
}
