﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Article;
using Iteyka.Domain;

namespace Iteyka.Application.Repositories
{
    public interface IArticleRepository
    {
        Task<Article> FindById(Guid id, CancellationToken cancellationToken);

        Task Add(Article article, CancellationToken cancellationToken);

        Task<(IEnumerable<Domain.Article>, int)> FindArticles(GetArticlesWithPagingRequestDto withPagingRequest, CancellationToken cancellationToken);
    }
}
