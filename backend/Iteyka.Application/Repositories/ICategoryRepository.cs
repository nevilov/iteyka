﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Iteyka.Application.Repositories
{
    public interface ICategoryRepository
    {
        Task<IEnumerable<Domain.Category>> GetAll(CancellationToken cancellationToken);

        Task<Domain.Category> FindById(Guid id, CancellationToken cancellationToken);

        Task<Domain.Category> FindBySlug(string slug, CancellationToken cancellationToken);
    }
}
