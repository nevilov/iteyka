﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Domain;

namespace Iteyka.Application.Repositories
{
    public interface ICommentRepository
    {
        Task Add(Comment comment, CancellationToken cancellationToken);

        Task<IEnumerable<Comment>> FindComments(Guid articleId, CancellationToken cancellationToken);
    }
}
