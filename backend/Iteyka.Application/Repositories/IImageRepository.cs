using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Domain;

namespace Iteyka.Application.Repositories
{
    public interface IImageRepository
    {
        Task Add(Image image, CancellationToken cancellationToken);

        Task<Image> FindById(Guid id, CancellationToken cancellationToken);

        void Delete(Image image, CancellationToken cancellationToken);
        Task<IEnumerable<Image>> GetImages(Guid source, CancellationToken cancellationToken);
    }
}
