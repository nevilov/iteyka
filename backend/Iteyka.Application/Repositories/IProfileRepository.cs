﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Application.Repositories
{
    public interface IProfileRepository
    {
        Task Create(Guid id, CreateProfileRequest request, CancellationToken cancellationToken);

        Task<Domain.Profile> FindByUserName(string userName, CancellationToken cancellationToken);

        Task<Domain.Profile> FindById(Guid id, CancellationToken cancellationToken);

        Task<IEnumerable<Domain.Profile>> FindUsers(GetUsersRequest request,
            CancellationToken cancellationToken);
    }
}
