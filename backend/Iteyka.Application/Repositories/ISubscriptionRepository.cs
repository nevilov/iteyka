﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Domain;

namespace Iteyka.Application.Repositories
{
    public interface ISubscriptionRepository
    {
        Task Subscribe(Subscription subscription, CancellationToken cancellationToken);

        void Unsubscribe(Subscription subscription, CancellationToken cancellationToken);

        Task<Subscription> GetById(Guid id, CancellationToken cancellationToken);

        Task<Subscription> GetUserSubscriptionBySource(Guid source, Guid userId, CancellationToken cancellationToken);

        Task<int> GetSubscribersCount(Guid source, CancellationToken cancellationToken);
    }
}
