using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Domain;

namespace Iteyka.Application.Repositories
{
    public interface IMessageRepository
    {
        Task Send(Message message, CancellationToken cancellationToken);

        Task<IEnumerable<Message>> FindByChatId(Guid chatId, CancellationToken cancellationToken);
    }
}
