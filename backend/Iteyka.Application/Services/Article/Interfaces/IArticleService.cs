﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Article;

namespace Iteyka.Application.Services.Article.Interfaces
{
    public interface IArticleService
    {
        public Task<Guid> Create(ArticleRequestDto request, CancellationToken cancellationToken);

        public Task<GetArticleByIdResponseDto> GetById(Guid id, CancellationToken cancellationToken);

        public Task Update(Guid id,ArticleRequestDto request, CancellationToken cancellationToken);

        public Task Remove(Guid id, CancellationToken cancellationToken);

        public Task<GetArticlesResponse> Get(GetArticlesWithPagingRequestDto withPagingRequest,
            CancellationToken cancellationToken);
    }
}
