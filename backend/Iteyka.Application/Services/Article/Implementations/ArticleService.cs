﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Article.Interfaces;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Article;
using Iteyka.Contracts.Dtos.Profile;
using Iteyka.Domain.Shared.Exceptions;

namespace Iteyka.Application.Services.Article.Implementations
{
    public class ArticleService : IArticleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIdentityService _identityService;

        public ArticleService(IUnitOfWork unitOfWork
            , IIdentityService identityService)
        {
            _unitOfWork = unitOfWork;
            _identityService = identityService;
        }

        public async Task<Guid> Create(ArticleRequestDto request, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);
            var user = await _unitOfWork.ProfileRepository.FindById(userId, cancellationToken);

            var article = new Domain.Article
            {
                Title = request.Title,
                Content = request.Content,
                CategoryId = request.CategoryId,
                Author = user,
                AuthorId = user.Id
            };

            await _unitOfWork.ArticleRepository.Add(article, cancellationToken);
            await _unitOfWork.SaveChanges(cancellationToken);
            return article.Id;
        }

        public async Task<GetArticleByIdResponseDto> GetById(Guid id, CancellationToken cancellationToken)
        {
            var article = await _unitOfWork.ArticleRepository.FindById(id, cancellationToken);
            if(article is null)
            {
                throw new NotFoundException("Article was not found");
            }

            return new GetArticleByIdResponseDto()
            {
                Id = article.Id,
                Title = article.Title,
                Content = article.Content,
                CreatedDate = article.CreatedDate,
                Category = new CategoryResponse
                {
                    Id = article.Category.Id,
                    Name = article.Category.Name,
                    Slug = article.Category.Slug
                },
                Author = new UserDto
                {
                    Id = article.Author.Id,
                    Name = article.Author.Name,
                    LastName = article.Author.LastName,
                    UserName = article.Author.UserName
                },
            };
        }

        public async Task Update(Guid id, ArticleRequestDto request, CancellationToken cancellationToken)
        {
            var article = await _unitOfWork.ArticleRepository.FindById(id, cancellationToken);

            if (article == null)
            {
                throw new NotFoundException("Article was not found");
            }

            article.Title = request.Title;
            article.Content = request.Content;
            article.UpdatedDate = DateTime.UtcNow;
            article.CategoryId = request.CategoryId;

            await _unitOfWork.SaveChanges(cancellationToken);
        }

        public async Task Remove(Guid id, CancellationToken cancellationToken)
        {
            var article = await _unitOfWork.ArticleRepository.FindById(id, cancellationToken);

            if (article == null)
            {
                throw new NotFoundException("Article was not found");
            }

            article.IsDeleted = true;
            article.RemovedDate = DateTime.UtcNow;

            await _unitOfWork.SaveChanges(cancellationToken);
        }

        public async Task<GetArticlesResponse> Get(GetArticlesWithPagingRequestDto withPagingRequest, CancellationToken cancellationToken)
        {
            var (articles, total) = await _unitOfWork.ArticleRepository.FindArticles(withPagingRequest, cancellationToken);

            return new GetArticlesResponse
                {
                    Items = articles.Select(a => new ArticleDto
                    {
                        Id = a.Id,
                        Title = a.Title,
                        Content = a.Content,
                        CreatedDate = a.CreatedDate,
                        Author = new UserDto
                        {
                            Id = a.Author.Id,
                            Name = a.Author.Name,
                            LastName = a.Author.LastName,
                            UserName = a.Author.UserName
                        }
                    }),
                    Limit = withPagingRequest.Limit,
                    Offset = withPagingRequest.Offset,
                    Total = total
                };
        }
    }
}
