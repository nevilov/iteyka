﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Category;

namespace Iteyka.Application.Services.Category.Interfaces
{
    public interface ICategoryService
    {
        Task<CategoryResponseDto> GetAllCategories(CancellationToken cancellationToken);

        Task<CategoryDto> GetBySlug(string slug, CancellationToken cancellationToken);
    }
}
