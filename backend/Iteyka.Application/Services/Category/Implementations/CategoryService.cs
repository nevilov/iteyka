﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Category.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Article;
using Iteyka.Contracts.Dtos.Category;
using Iteyka.Domain.Shared.Exceptions;

namespace Iteyka.Application.Services.Category.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly IUnitOfWork _unitOfWork;

        public CategoryService(IUnitOfWork unitOfWork) => _unitOfWork = unitOfWork;

        public async Task<CategoryResponseDto> GetAllCategories(CancellationToken cancellationToken)
        {
            var categories = await _unitOfWork.CategoryRepository.GetAll(cancellationToken);
            return new CategoryResponseDto
            {
                Categories = categories.Select(a => new CategoryDto()
                {
                    Id = a.Id,
                    Name = a.Name,
                    Slug = a.Slug,
                    CreatedDate = a.CreatedDate,
                    ChildCategories = a.ChildCategories.Select(c => new CategoryDto()
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Slug = c.Slug,
                        CreatedDate = c.CreatedDate
                    }),
                })
            };
        }

        public async Task<CategoryDto> GetBySlug(string slug, CancellationToken cancellationToken)
        {
            var category = await _unitOfWork.CategoryRepository.FindBySlug(slug, cancellationToken);
            var image = await _unitOfWork.ImageRepository.GetImages(category.Id, cancellationToken);
            return new CategoryDto()
            {
                Id = category.Id,
                Name = category.Name,
                CreatedDate = category.CreatedDate,
                Slug = category.Slug,
                Image = image.FirstOrDefault().ImageBlob
            };
        }
    }
}
