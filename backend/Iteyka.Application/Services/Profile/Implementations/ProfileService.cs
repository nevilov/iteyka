﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.Services.Profile.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Common.Sort;
using Iteyka.Contracts.Dtos.Identity;
using Iteyka.Contracts.Dtos.Profile;
using Iteyka.Domain.Shared.Exceptions;

namespace Iteyka.Application.Services.Profile.Implementations
{
    public class ProfileService : IProfileService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIdentityService _identityService;

        public ProfileService(
            IUnitOfWork unitOfWork
            , IIdentityService identityService
            )
        {
            _unitOfWork = unitOfWork;
            _identityService = identityService;
        }

        public async Task<Guid> CreateUser(CreateProfileRequest request, CancellationToken cancellationToken)
        {
            var existedUser = await _identityService.FindUserByEmail(request.Email, cancellationToken);
            if(existedUser != null)
            {
                throw new DuplicateException($"User with email {request.Email} already exist");
            }

            var identityUserCreatedResultId = await _identityService.CreateUser(new RegisterRequest
            {
                Email = request.Email,
                UserName = request.UserName,
                Password = request.Password
            }, cancellationToken);

            await _unitOfWork.ProfileRepository.Create(identityUserCreatedResultId, request, cancellationToken);
            await _unitOfWork.SaveChanges(cancellationToken);

            return identityUserCreatedResultId;
        }

        public async Task<UserDto> GetByUserName(string userName, CancellationToken cancellationToken)
        {
            var user = await _unitOfWork.ProfileRepository.FindByUserName(userName, cancellationToken);
            if(user == null)
            {
                throw new NotFoundException($"User {userName} was not found");
            }
            var userRole = await _identityService.GetUserRole(user.Id, cancellationToken);

            return new UserDto
            {
                Id = user.Id,
                Name = user.Name,
                LastName = user.LastName,
                UserName = user.UserName,
                RoleName = userRole.Name,
                CreatedDate = user.CreatedDate
            };
        }

        public async Task<UserDto> GetCurrentUser(CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);

            var user = await _unitOfWork.ProfileRepository.FindById(userId, cancellationToken);

            var userRole = await _identityService.GetUserRole(user.Id, cancellationToken);
            return new UserDto
            {
                Id = user.Id,
                Name = user.Name,
                UserName = user.UserName,
                LastName = user.LastName,
                RoleName = userRole.Name,
                CreatedDate = user.CreatedDate
            };
        }

        public async Task<PagedResponse<UserDto>> GetUsers(GetUsersRequest request, CancellationToken cancellationToken)
        {
            var users = await _unitOfWork.ProfileRepository.FindUsers(request, cancellationToken);

            return new PagedResponse<UserDto>
            {
                Items = users.Select(a => new UserDto()
                {
                    Name = a.Name, LastName = a.LastName, UserName = a.UserName, CreatedDate = a.CreatedDate
                }),
                Total = users.Count(),
                Offset = request.Offset,
                Limit = request.Limit
            };
        }

        public async Task UpdateUser(UserDto userDto, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);
            var user = await _unitOfWork.ProfileRepository.FindById(userId, cancellationToken);

            user.Name = userDto.Name;
            user.LastName = userDto.LastName;
            user.UpdatedDate = DateTime.UtcNow;

            await _unitOfWork.SaveChanges(cancellationToken);
        }

        public async Task UpdateStatus(string status, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);
            var profile = await _unitOfWork.ProfileRepository.FindById(userId, cancellationToken);

            profile.Status = status;

            await _unitOfWork.SaveChanges(cancellationToken);
        }
    }
}
