﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Common.Sort;
using Iteyka.Contracts.Dtos.Profile;

namespace Iteyka.Application.Services.Profile.Interfaces
{
    public interface IProfileService
    {
        Task<UserDto> GetByUserName(string userName, CancellationToken cancellationToken);

        Task<Guid> CreateUser(CreateProfileRequest request, CancellationToken cancellationToken);

        Task<UserDto> GetCurrentUser(CancellationToken cancellationToken);

        Task<PagedResponse<UserDto>> GetUsers(GetUsersRequest request, CancellationToken cancellationToken);

        Task UpdateUser(UserDto userDto, CancellationToken cancellationToken);

        Task UpdateStatus(string status, CancellationToken cancellationToken);
    }
}
