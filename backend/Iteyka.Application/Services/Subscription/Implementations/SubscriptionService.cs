﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.Services.Subscription.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Subscription;
using Iteyka.Domain.Shared.Exceptions;

namespace Iteyka.Application.Services.Subscription.Implementations
{
    public class SubscriptionService : ISubscriptionService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIdentityService _identityService;

        public SubscriptionService(IUnitOfWork unitOfWork, IIdentityService identityService)
        {
            _unitOfWork = unitOfWork;
            _identityService = identityService;
        }


        public async Task<SubscriptionDto> GetSubscription(Guid id, CancellationToken cancellationToken)
        {
            var subscription = await _unitOfWork.SubscriptionRepository.GetById(id, cancellationToken);

            if (subscription is null)
            {
                throw new NotFoundException($"Subscription was not found. Id {id}");
            }

            return new SubscriptionDto
            {
                Id = id,
                Source = subscription.Source,
                SubscriberId = subscription.SubscriberId,
                Type = subscription.Type
            };
        }

        public async Task Subscribe(CreateSubscriptionDto createSubscriptionDto, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);

            if(userId == createSubscriptionDto.Source)
            {
                throw new NoRightsException("Conflict!");

            }

            var subscription = await _unitOfWork.SubscriptionRepository.GetUserSubscriptionBySource(createSubscriptionDto.Source, userId, cancellationToken);
            if(subscription is not null)
            {
                throw new DuplicateException("User already subscribed!");
            }

            await _unitOfWork.SubscriptionRepository.Subscribe(new Domain.Subscription
            {
                Source = createSubscriptionDto.Source,
                SubscriberId = userId,
                Type = createSubscriptionDto.Type
            }, cancellationToken);

            await _unitOfWork.SaveChanges(cancellationToken);
        }

        public async Task Unsubscribe(Guid source, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);
            var subscription = await _unitOfWork.SubscriptionRepository.GetUserSubscriptionBySource(source, userId, cancellationToken);
            _unitOfWork.SubscriptionRepository.Unsubscribe(subscription, cancellationToken);
            await _unitOfWork.SaveChanges(cancellationToken);
        }

        public async Task<SubscriptionDto> GetUserSubscriptionBySource(Guid source, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);

            var subscription = await _unitOfWork.SubscriptionRepository.GetUserSubscriptionBySource(source, userId, cancellationToken);

            if (subscription is null)
            {
                throw new NotFoundException($"Subscription was not found. Source {source}, userId {userId}");
            }

            return new SubscriptionDto()
            {
                Id = subscription.Id,
                Source = source,
                SubscriberId = subscription.SubscriberId,
                Type = subscription.Type
            };
        }

        public async Task<int> GetSubscribersCount(Guid source, CancellationToken cancellationToken)
        {
            return await _unitOfWork.SubscriptionRepository.GetSubscribersCount(source, cancellationToken);
        }
    }
}
