﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Subscription;

namespace Iteyka.Application.Services.Subscription.Interfaces
{
    public interface ISubscriptionService
    {
        Task Subscribe(CreateSubscriptionDto createSubscriptionDto, CancellationToken cancellationToken);

        Task Unsubscribe(Guid sorce, CancellationToken cancellationToken);

        Task<SubscriptionDto> GetSubscription(Guid id, CancellationToken cancellationToken);

        Task<SubscriptionDto> GetUserSubscriptionBySource(Guid source, CancellationToken cancellationToken);

        Task<int> GetSubscribersCount(Guid source, CancellationToken cancellationToken);
    }
}
