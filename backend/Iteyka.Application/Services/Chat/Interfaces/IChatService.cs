using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Chat;

namespace Iteyka.Application.Services.Chat.Interfaces
{
    public interface IChatService
    {
        Task<ChatDto> GetById(Guid id, CancellationToken cancellationToken);

        Task<IEnumerable<ChatListItemDto>> GetUserChats(CancellationToken cancellationToken);

        Task CreateChat(CreateChatDto dto, CancellationToken cancellationToken);
    }
}
