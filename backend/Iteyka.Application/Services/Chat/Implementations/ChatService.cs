using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Chat.Interfaces;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Chat;
using Iteyka.Contracts.Dtos.Message;
using Iteyka.Contracts.Dtos.Profile;
using Iteyka.Domain.Shared.Exceptions;

namespace Iteyka.Application.Services.Chat.Implementations
{
    public class ChatService : IChatService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIdentityService _identityService;

        public ChatService(IUnitOfWork unitOfWork, IIdentityService identityService)
        {
            _unitOfWork = unitOfWork;
            _identityService = identityService;
        }

        public async Task<ChatDto> GetById(Guid id, CancellationToken cancellationToken)
        {
            var chat = await _unitOfWork.ChatRepository.FindById(id, cancellationToken);
            if (chat is null)
            {
                throw new NotFoundException($"Chat {id} was not found");
            }

            return new ChatDto()
            {
                Id = chat.Id,
                Name = chat.Name,
                CreatedDate = chat.CreatedDate,
                Companion =
                    new UserDto()
                    {
                        Id = chat.CompanionId,
                        LastName = chat.Companion.LastName,
                        Name = chat.Companion.Name,
                        UserName = chat.Companion.UserName
                    },
                Creator = new UserDto()
                {
                    Id = chat.CreatorId,
                    Name = chat.Creator.Name,
                    LastName = chat.Creator.LastName,
                    UserName = chat.Creator.UserName
                }
            };
        }

        public async Task<IEnumerable<ChatListItemDto>> GetUserChats(CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);

            var chats = await _unitOfWork.ChatRepository.FindUserChats(userId, cancellationToken);

            return chats.Select(a => new ChatListItemDto()
            {
                Id = a.Id,
                Name = a.Name,
                CreatedDate = a.CreatedDate,
                Companion =
                    new UserDto()
                    {
                        Id = a.CompanionId,
                        LastName = a.Companion.LastName,
                        Name = a.Companion.Name,
                        UserName = a.Companion.UserName
                    },
                Creator = new UserDto()
                {
                    Id = a.CreatorId,
                    Name = a.Creator.Name,
                    LastName = a.Creator.LastName,
                    UserName = a.Creator.UserName
                },
            });
        }

        public async Task CreateChat(CreateChatDto dto, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);

            var chat = new Domain.Chat
            {
                Name = Guid.NewGuid().ToString(),
                CompanionId = dto.CompanionId,
                CreatorId = userId,
            };

            await _unitOfWork.ChatRepository.CreateChat(chat, cancellationToken);
            await _unitOfWork.SaveChanges(cancellationToken);
        }
    }
}
