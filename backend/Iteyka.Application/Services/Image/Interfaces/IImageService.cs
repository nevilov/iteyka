using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Image;
using Iteyka.Domain;
using Microsoft.AspNetCore.Http;

namespace Iteyka.Application.Services.Image.Interfaces
{
    public interface IImageService
    {
        Task Upload(Guid source, IFormFile image, CancellationToken cancellationToken);

        Task<ImageDto> GetById(Guid id, CancellationToken cancellationToken);

        Task Delete(Guid id, CancellationToken cancellationToken);

        Task<IEnumerable<ImageDto>> GetArticleImages(Guid articleId, CancellationToken cancellationToken);

        Task<ImageDto> GetBySource(Guid source, CancellationToken cancellationToken);

        Task UpdateAvatar(Guid id, IFormFile file, CancellationToken cancellationToken);
    }
}
