using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.Services.Message.Interfaces;
using Iteyka.Application.SignalR;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Chat;
using Iteyka.Contracts.Dtos.Message;
using Iteyka.Domain.Shared.Exceptions;

namespace Iteyka.Application.Services.Message.Implementations
{
    public class MessageService : IMessageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIdentityService _identityService;
        private readonly ISignalRService _signalRService;

        public MessageService(IUnitOfWork unitOfWork,
            IIdentityService identityService,
            ISignalRService signalRService)
        {
            _unitOfWork = unitOfWork;
            _identityService = identityService;
            _signalRService = signalRService;
        }

        public async Task Send(CreateMessageDto dto, CancellationToken cancellationToken)
        {
            var chat = await _unitOfWork.ChatRepository.FindById(dto.ChatId, cancellationToken);

            if (chat is null)
            {
                throw new NotFoundException($"Chat {dto.ChatId} was not found");
            }

            var userId = await _identityService.GetCurrentUserId(cancellationToken);

            var message = new Domain.Message() {ChatId = dto.ChatId, Content = dto.Content, SenderId = userId, RecipientId = dto.RecipientId};

            await _unitOfWork.MessageRepository.Send(message, cancellationToken);

            await _unitOfWork.SaveChanges(cancellationToken);

            await _signalRService.Send(new Contracts.Dtos.SignalR.MessageDto
            {
                Id = message.Id.ToString(),
                SenderId = message.SenderId.ToString(),
                CreatedDate = message.CreatedDate,
                Content = message.Content,
                RecipientId = message.RecipientId.ToString(),
            }, cancellationToken);
        }

        public async Task<IEnumerable<MessageDto>> GetByChatId(Guid chatId, CancellationToken cancellationToken)
        {
            var userId = await _identityService.GetCurrentUserId(cancellationToken);
            var chat = await _unitOfWork.ChatRepository.FindById(chatId, cancellationToken);
            if(chat is null)
            {
                throw new NotFoundException($"Chat {chatId} was not found");
            }
            if (chat.Companion.Id != userId && chat.Creator.Id != userId)
            {
                throw new NoRightsException($"User {userId} have no rights to get chat messages");
            }

            var messages = await _unitOfWork.MessageRepository.FindByChatId(chatId, cancellationToken);
            return messages.Select(a => new MessageDto
            {
                Id = a.Id,
                SenderId = a.SenderId,
                Content = a.Content,
                CreatedDate = a.CreatedDate,
                RecipientId = a.RecipientId
            });
        }
    }
}
