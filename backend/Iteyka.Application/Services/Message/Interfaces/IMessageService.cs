﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Message;

namespace Iteyka.Application.Services.Message.Interfaces
{
    public interface IMessageService
    {
        Task Send(CreateMessageDto dto, CancellationToken cancellationToken);

        Task<IEnumerable<MessageDto>> GetByChatId(Guid chatId, CancellationToken cancellationToken);
    }
}
