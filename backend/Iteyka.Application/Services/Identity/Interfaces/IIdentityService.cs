﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Identity;
using Iteyka.Contracts.Dtos.Profile;
using Iteyka.Domain;

namespace Iteyka.Application.Services.Identity.Interfaces
{
    public interface IIdentityService
    {
        Task ChangeRole(ChangeRoleDto dto, CancellationToken cancellationToken);

        Task<Guid> GetCurrentUserId(CancellationToken cancellationToken = default);

        Task<LoginResponse> Login(LoginRequest request, CancellationToken cancellationToken);

        Task<Guid> CreateUser(RegisterRequest request, CancellationToken cancellationToken);

        Task<ApplicationUser> FindUserByEmail(string email, CancellationToken cancellationToken = default);

        Task<ApplicationUser> FindUserById(Guid id, CancellationToken cancellationToken);

        Task<bool> IsInRole(Guid userId, string role, CancellationToken cancellationToken);

        Task<ApplicationRole> GetUserRole(Guid userId, CancellationToken cancellationToken);

        Task Block(BlockDto dto, CancellationToken cancellationToken);
    }
}
