﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.Comment;

namespace Iteyka.Application.Services.Comment.Interfaces
{
    public interface ICommentService
    {
        Task<GetCommentsResponseDto> Get(Guid articleId, CancellationToken cancellationToken);

        Task Add(Guid articleId, CommentRequestDto request, CancellationToken cancellationToken);
    }
}
