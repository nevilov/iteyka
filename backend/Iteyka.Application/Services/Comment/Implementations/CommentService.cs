﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Services.Comment.Interfaces;
using Iteyka.Application.Services.Identity.Interfaces;
using Iteyka.Application.UnitOfWork;
using Iteyka.Contracts.Dtos.Comment;
using Iteyka.Contracts.Dtos.Profile;
using Iteyka.Domain.Shared.Exceptions;

namespace Iteyka.Application.Services.Comment.Implementations
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IIdentityService _identityService;

        public CommentService(IUnitOfWork unitOfWork
            , IIdentityService identityService)
        {
            _unitOfWork = unitOfWork;
            _identityService = identityService;
        }

        public async Task Add(Guid articleId, CommentRequestDto request, CancellationToken cancellationToken)
        {
            var article = await _unitOfWork.ArticleRepository.FindById(articleId, cancellationToken);
            if(article == null)
            {
                throw new NotFoundException($"Article (id {articleId}) was not found");
            }

            var userId = await _identityService.GetCurrentUserId(cancellationToken);
            var user = await _unitOfWork.ProfileRepository.FindById(userId, cancellationToken);

            var comment = new Domain.Comment
            {
                ArticleId = articleId,
                Content = request.Content,
                Author = user,
                AuthorId = user.Id
            };

            await _unitOfWork.CommentRepository.Add(comment, cancellationToken);
            await _unitOfWork.SaveChanges(cancellationToken);
        }

        public async Task<GetCommentsResponseDto> Get(Guid articleId, CancellationToken cancellationToken)
        {
            var article = await _unitOfWork.ArticleRepository.FindById(articleId, cancellationToken);
            if (article == null)
            {
                throw new NotFoundException($"Article (id {articleId}) was not found");
            }

            var comments = await _unitOfWork.CommentRepository.FindComments(articleId, cancellationToken);

            return new GetCommentsResponseDto
            {
                Comments = comments.Select(c => new GetCommentsResponseDto.CommentResponse
                {
                    Content = c.Content,
                    Author = new UserDto
                    {
                        UserName = c.Author.UserName,
                        Name = c.Author.Name,
                        LastName = c.Author.LastName
                    }
                }).ToList()
            };
        }
    }
}
