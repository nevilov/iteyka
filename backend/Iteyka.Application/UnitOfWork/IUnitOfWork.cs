﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Iteyka.Application.Repositories;

namespace Iteyka.Application.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        public Task SaveChanges(CancellationToken cancellationToken);

        public IArticleRepository ArticleRepository { get; }
        public ICommentRepository CommentRepository { get; }
        public ICategoryRepository CategoryRepository { get; }
        public IProfileRepository ProfileRepository { get; }
        public ISubscriptionRepository SubscriptionRepository { get; }
        public IImageRepository ImageRepository { get; }
        public IChatRepository ChatRepository { get; }
        public IMessageRepository MessageRepository { get; }
    }
}
