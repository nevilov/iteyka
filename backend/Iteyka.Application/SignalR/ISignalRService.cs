﻿using System.Threading;
using System.Threading.Tasks;
using Iteyka.Contracts.Dtos.SignalR;

namespace Iteyka.Application.SignalR
{
    public interface ISignalRService
    {
        Task Send(MessageDto message, CancellationToken cancellationToken);
    }
}
